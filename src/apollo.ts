import { ApolloClient } from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import { createUploadLink } from 'apollo-upload-client';
import { split } from 'apollo-link';
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';

// import { Platform } from 'react-native';

const host = "https://bakbak-heroku.herokuapp.com";
const wshost = "wss://bakbak-heroku.herokuapp.com";

// let host;
// let wshost;

// if (process.env.REACT_APP_SERVER_URL) {
//   host = process.env.REACT_APP_SERVER_URL;
//   wshost = process.env.REACT_APP_SERVER_WS_URL as string;
// } else {
//   host = Platform.OS === 'ios' ? 'http://localhost:4000' : 'http://172.20.10.7:4000';
//   wshost = Platform.OS === 'ios' ? 'ws://localhost:4000' : 'ws://172.20.10.7:4000';
// }
// const host = Platform.OS === 'ios' ? 'http://localhost:4000' : 'http://192.168.0.16:4000';
// const wshost = Platform.OS === 'ios' ? 'ws://localhost:4000' : 'ws://192.168.0.16:4000';

// const host = Platform.OS === 'ios' ? 'http://localhost:4000' : 'http://192.168.0.16:4000';
// const wshost = Platform.OS === 'ios' ? 'ws://localhost:4000' : 'ws://192.168.0.16:4000';

const httpLink = createUploadLink({
  uri: host,
  credentials: "include"
});

// Create a WebSocket link:
const wsLink = new WebSocketLink({
  uri: wshost,
  options: {
    reconnect: true
  }
});

// using the ability to split links, you can send data to each link
// depending on what kind of operation is being sent
const link = split(
  // split based on operation type
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query) as any;
    return kind === 'OperationDefinition' && operation === 'subscription';
  },
  wsLink,
  httpLink,
);

export const client = new ApolloClient({
  link,
  cache: new InMemoryCache()
});
