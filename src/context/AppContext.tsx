import * as React from 'react';

import { COMMENT_SORT } from '@bakbak/common';
import { GetPostDetailsByIdQuery_getPostDetailsById } from '@bakbak/controller';

import { getLocalStorageItem, setLocalStorageItem, removeLocalStorageItem } from '../util/LocalStorage';
import { LOCAL_STORAGE_LOGGED_USER } from '../constants';

export interface ContextState {

  selectedPost: GetPostDetailsByIdQuery_getPostDetailsById | null;
  updateSelectedPost: (selectedPost: GetPostDetailsByIdQuery_getPostDetailsById) => void;

  showCommentModal: boolean;
  toggleCommentModal: () => void;

  showCommentSortFilter: boolean;
  toggleCommentSortFilter: () => void;

  commentSort: string;
  updateCommentSort: (commentSort: string) => void;

  loggedUser: any;
  updateLoggedUser: (loggedUser: any) => void;
}

const AppContext = React.createContext<ContextState>(null as any);
export const AppConsumer = AppContext.Consumer;

export class AppProvider extends React.Component<{}, ContextState> {
  state = {
    selectedPost: null,
    updateSelectedPost: (selectedPost: GetPostDetailsByIdQuery_getPostDetailsById) => {
      this.setState(() => ({ selectedPost }));
    },

    showCommentModal: false,
    toggleCommentModal: () => {
      this.setState((state) => ({ showCommentModal: !state.showCommentModal }));
    },

    showCommentSortFilter: false,
    toggleCommentSortFilter: () => {
      this.setState((state) => ({ showCommentSortFilter: !state.showCommentSortFilter }));
    },

    commentSort: COMMENT_SORT[0].key,
    updateCommentSort: (commentSort: string) => this.setState(() => ({ commentSort })),

    loggedUser: null,
    updateLoggedUser: async (loggedUser: any) => {
      this.setState(() => ({ loggedUser }));
      if (loggedUser === null) {
        await removeLocalStorageItem(LOCAL_STORAGE_LOGGED_USER);
      } else {
        await setLocalStorageItem(LOCAL_STORAGE_LOGGED_USER, loggedUser);
      }
    },
  }

  async componentDidMount() {
    const loggedUser = await getLocalStorageItem(LOCAL_STORAGE_LOGGED_USER);
    if (loggedUser) {
      this.setState(() => ({ loggedUser }));
    }
  }

  render() {
    return (
      <AppContext.Provider value={this.state}>
        {this.props.children}
      </AppContext.Provider>
    )
  }
}

export const withAppContext = (Component: any) => (props: any) => (
  <AppConsumer>
    {(context: ContextState | null) => <Component {...props} context={context} />}
  </AppConsumer>
);

export interface AppContextProps {
  context: ContextState;
} 