import * as React from 'react';
import { CATEGORY_FILTER, DATE_FILTER, SCORE_SORT, SORT_TYPE } from '@bakbak/common';

import { setLocalStorageItem, getLocalStorageItem } from '../util/LocalStorage';
import { LOCAL_STORAGE_CATEGORY_FILTER, LOCAL_STORAGE_DATE_FILTER, LOCAL_STORAGE_DATE_SORT, LOCAL_STORAGE_SCORE_SORT, LOCAL_STORAGE_SORT_TYPE, LOCAL_STORAGE_SHOW_REFRESH } from '../constants';

export interface HeaderContext {
  searchText: string;
  updateSearchText: (searchText: string) => void;

  showFilter: boolean;
  toggleShowFilter: () => void;

  showSort: boolean;
  toggleShowSort: () => void;

  categoryFilter: string[];
  updateCategoryFilter: (categoryFilter: string[]) => void;

  dateFilter: string[];
  updateDateFilter: (dateFilter: string[]) => void;

  dateSort: string[] | null;
  updateDateSort: (dateSort: string[]) => void;

  scoreSort: string[] | null;
  updateScoreSort: (scoreSort: string[]) => void;

  sortType: string;
  updateSortType: (sortType: string) => void;

  displayPullToRefresh: boolean;
  toggleDisplayPullToRefresh: () => void;

}

const HeaderContext = React.createContext<HeaderContext>(null as any);
export const HeaderConsumer = HeaderContext.Consumer;

export class HeaderProvider extends React.Component<{}, HeaderContext> {
  state = {
    searchText: '',
    updateSearchText: (searchText: string) => this.setState({ searchText }),

    showFilter: false,
    toggleShowFilter: () => this.setState((state) => ({ showFilter: !state.showFilter })),

    showSort: false,
    toggleShowSort: () => this.setState((state) => ({ showSort: !state.showSort })),

    categoryFilter: [],
    updateCategoryFilter: async (categoryFilter: string[]) => {
      await setLocalStorageItem(LOCAL_STORAGE_CATEGORY_FILTER, categoryFilter);
      this.setState(() => ({ categoryFilter: [...categoryFilter] }));
    },

    dateFilter: [],
    updateDateFilter: async (dateFilter: string[]) => {
      await setLocalStorageItem(LOCAL_STORAGE_DATE_FILTER, dateFilter);
      this.setState(() => (({ dateFilter: [...dateFilter] })));
    },

    dateSort: [],
    updateDateSort: async (dateSort: string[]) => {
      await setLocalStorageItem(LOCAL_STORAGE_DATE_SORT, dateSort);
      this.setState(() => (({ dateSort: [...dateSort], scoreSort: null })));
      await setLocalStorageItem(LOCAL_STORAGE_SCORE_SORT, null);
    },

    scoreSort: [],
    updateScoreSort: async (scoreSort: string[]) => {
      await setLocalStorageItem(LOCAL_STORAGE_SCORE_SORT, scoreSort);
      this.setState(() => (({ scoreSort: [...scoreSort], dateSort: null })));
      await setLocalStorageItem(LOCAL_STORAGE_DATE_SORT, null);
    },

    sortType: SORT_TYPE[0].name,
    updateSortType: async (sortType: string) => {
      await setLocalStorageItem(LOCAL_STORAGE_SORT_TYPE, sortType);
      if (sortType === SORT_TYPE[0].value) {
        this.state.updateScoreSort(["DESC"]);
      } else if (sortType === SORT_TYPE[1].value) {
        this.state.updateDateSort(["DESC"]);
      }
      this.setState(() => ({ sortType }));
    },

    displayPullToRefresh: true,
    toggleDisplayPullToRefresh: () => {
      const displayPullToRefresh = !this.state.displayPullToRefresh;
      setLocalStorageItem(LOCAL_STORAGE_SHOW_REFRESH, displayPullToRefresh);
      this.setState(() => ({ displayPullToRefresh }));
    },
  }

  async componentDidMount() {
    const categoryFilter = await getLocalStorageItem(LOCAL_STORAGE_CATEGORY_FILTER);
    const dateFilter = await getLocalStorageItem(LOCAL_STORAGE_DATE_FILTER);
    const dateSort = await getLocalStorageItem(LOCAL_STORAGE_DATE_SORT);
    const scoreSort = await getLocalStorageItem(LOCAL_STORAGE_SCORE_SORT);
    const displayPullToRefresh = await getLocalStorageItem(LOCAL_STORAGE_SHOW_REFRESH);

    if (categoryFilter && categoryFilter.length !== 0) {
      this.setState(() => ({ categoryFilter: [...categoryFilter] }));
    } else {
      this.setState(() => ({ categoryFilter: [CATEGORY_FILTER[0].value] }));
    }
    if (dateFilter && dateFilter.length !== 0) {
      this.setState(() => ({ dateFilter: [...dateFilter] }));
    } else {
      this.setState(() => ({ dateFilter: [DATE_FILTER[1].value] }));
    }
    if (scoreSort && scoreSort.length !== 0) {
      this.setState(() => ({ scoreSort: [...scoreSort] }));
    } else if (dateSort && dateSort.length !== 0) {
      this.setState(() => ({ dateSort: [...dateSort] }));
    } else {
      this.setState(() => ({ scoreSort: [SCORE_SORT[0].value] }));
    }
    if (displayPullToRefresh !== null && displayPullToRefresh !== undefined) {
      this.setState(() => ({ displayPullToRefresh }));
    } else {
      this.setState(() => ({ displayPullToRefresh: true }));
    }
  }

  render() {
    return (
      <HeaderContext.Provider value={this.state}>
        {this.props.children}
      </HeaderContext.Provider>
    )
  }
}

export const withHeaderContext = (Component: any) => (props: any) => (
  <HeaderConsumer>
    {(context: HeaderContext | null) => <Component {...props} headerContext={context} />}
  </HeaderConsumer>
);

export interface HeaderContextProps {
  headerContext: HeaderContext;
} 