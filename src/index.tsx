import * as React from 'react';
import { Asset } from 'expo';
import { ApolloProvider } from 'react-apollo';
import { MenuProvider } from 'react-native-popup-menu';

import { client } from './apollo';
import Routes from './routes';
import { AppProvider } from "./context/AppContext";
import { HeaderProvider } from './context/HeaderContext';
// import { Image, View } from 'react-native';


export default class App extends React.PureComponent {
	state = {
		isSplashReady: false,
		isAppReady: false,
	};

	cacheSplashResourcesAsync = async () => {
		const gif = require('../assets/images/splash.png');
		return Asset.fromModule(gif).downloadAsync()
	}
	cacheResourcesAsync = async () => {
		// SplashScreen.hide();
		const images = [
			require('../assets/images/icon.png'),
		];

		const cacheImages = images.map((image) => {
			return Asset.fromModule(image).downloadAsync();
		});

		await Promise.all(cacheImages);
		this.setState({ isAppReady: true });
	}
	render() {
		// if (!this.state.isSplashReady) {
		//   return (
		//     <AppLoading
		//       startAsync={this.cacheSplashResourcesAsync}
		//       onFinish={() => this.setState({ isSplashReady: true })}
		//       onError={console.warn}
		//       autoHideSplash={false}
		//     />
		//   );
		// }
		// if (!this.state.isAppReady) {
		//   return (
		//     <View style={{ flex: 1 }}>
		//       <Image
		//         source={require('../assets/images/splash.gif')}
		//         onLoad={this.cacheResourcesAsync}
		//       />
		//     </View>
		//   );
		// }
		return (
			<ApolloProvider client={client}>
				<MenuProvider>
					<AppProvider>
						<HeaderProvider>
							<Routes />
						</HeaderProvider>
					</AppProvider>
				</MenuProvider>
			</ApolloProvider>
		);
	}
}