import * as React from 'react';
// import { SecureStore } from 'expo';
import { NavigationScreenProps } from 'react-navigation';

import { ROUTE_NAMES } from '@bakbak/common';
import { LoginController, LoginMutation_login_user } from '@bakbak/controller';

import { LoginView } from './LoginView';
import { withAppContext, AppContextProps } from '../../../context/AppContext';
// import { SESSION_ID_KEY } from '../../../constants';

class LoginC extends React.PureComponent<NavigationScreenProps & AppContextProps> {
	static navigationOptions = {
		title: 'Login here',
	};
	onComplete = (user: LoginMutation_login_user | null) => {
		if (user) {
			this.props.context.updateLoggedUser(user);
			this.props.navigation.navigate(ROUTE_NAMES.CATEGORY_LIST);
		} else {
			// TODO: did not get any response back.
		}
	}
	async componentDidMount() {
		if (this.props.context.loggedUser) {
			this.props.navigation.navigate(ROUTE_NAMES.CATEGORY_LIST);
		}
	}
	gotoRegister = () => {
		this.props.navigation.navigate(ROUTE_NAMES.REGISTER);
	}
	render() {
		return (
			<LoginController>
				{({ submit }) =>
					<LoginView
						onComplete={this.onComplete}
						submit={submit}
						gotoRegister={this.gotoRegister}
					/>
				}
			</LoginController>
		);
	}
}

export const LoginConnector = withAppContext(LoginC);