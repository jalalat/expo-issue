import * as React from "react";
import { View, Text, TouchableOpacity, KeyboardAvoidingView, StyleSheet } from "react-native";
import { Card, Button } from "react-native-elements";
import { withFormik, FormikProps, Field } from "formik";
import { MaterialIcons } from '@expo/vector-icons';

import { loginValidationSchema } from '@bakbak/common';
import { LoginMutationVariables, LoginMutation_login, LoginMutation_login_user } from '@bakbak/controller';

import { InputField } from "../../../util/InputField";
import { THEME_COLOR } from "../../../constants";

interface FormValues {
  email: string;
  password: string;
}

interface Props {
  onComplete: (user: LoginMutation_login_user | null) => void;
  gotoRegister: () => void;
  submit: (values: LoginMutationVariables) => Promise<LoginMutation_login | null>
}

interface State {
  secureText: boolean
}

class LoginForm extends React.PureComponent<FormikProps<FormValues> & Props, State> {
  state = { secureText: true };
  toggleSecureText = () => {
    this.setState((state) => ({ secureText: !state.secureText }));
  }
  render() {
    return (
      <KeyboardAvoidingView style={styles.keyboardStyle} behavior="padding">
        <Card>
          <Text style={{ fontSize: 20, marginBottom: 10, color: THEME_COLOR }}>Login</Text>
          <Field
            name="email"
            placeholder="email"
            component={InputField}
            containerStyle={{ width: '100%', borderBottomWidth: 1 }}
            autoCaptialize="none"
            keyboardType="email-address"
          />
          <View style={{ display: 'flex', flexDirection: 'row', justifyContent: "space-around" }}>
            <View style={{ width: "100%" }}>
              <Field
                name="password"
                placeholder="password"
                secureTextEntry={this.state.secureText}
                component={InputField}
                style={{ paddingRight: 20 }}
                containerStyle={{ width: '100%', borderBottomWidth: 1 }}
              />
            </View>
            <TouchableOpacity onPress={this.toggleSecureText} style={styles.secureTextButtonStyle}>
              <MaterialIcons name="remove-red-eye" size={20} color={THEME_COLOR} />
            </TouchableOpacity>
          </View>
          <Text style={{ marginBottom: 10 }} />
          <Button
            title="Submit"
            onPress={this.props.handleSubmit as any}
            buttonStyle={{ backgroundColor: THEME_COLOR }}
            fontSize={16}
            fontWeight={"600"}
          />
          <TouchableOpacity onPress={this.props.gotoRegister} style={{ paddingTop: 20, flexDirection: 'row', marginLeft: 15 }}>
            <Text>Create a new account </Text>
            <Text style={{ color: THEME_COLOR }}>Register Here</Text>
          </TouchableOpacity>
        </Card>
      </KeyboardAvoidingView>
    );
  }
}

export const LoginView = withFormik<Props, FormValues>({
  validationSchema: loginValidationSchema,
  mapPropsToValues: () => ({ email: "", userName: "", password: "" }),
  handleSubmit: async (values, { props, setErrors }) => {
    const response = await props.submit(values);
    if (response && response.errors) {
      setErrors(response.errors as any);
    } else if (response) {
      props.onComplete(response.user);
    }
  }
})(LoginForm);

const styles = StyleSheet.create({
  keyboardStyle: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
  },
  secureTextButtonStyle: {
    marginLeft: 'auto',
    marginRight: 15
  }
});  
