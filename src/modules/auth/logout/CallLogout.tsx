import * as React from 'react'
import { View } from 'react-native';

interface Props {
  logout: () => void;
  onComplete: () => void
}
export class CallLogout extends React.PureComponent<Props> {
  async componentDidMount() {
    await this.props.logout();
    await this.props.onComplete();
  }
  render() {
    return <View />;
  }
}