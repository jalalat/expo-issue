import * as React from 'react'
import { NavigationScreenProps } from 'react-navigation';

import { ROUTE_NAMES } from '@bakbak/common';
import { LogoutController } from '@bakbak/controller';

import { CallLogout } from './CallLogout';
import { withAppContext, AppContextProps } from '../../../context/AppContext';

class LogoutC extends React.PureComponent<NavigationScreenProps<{}> & AppContextProps> {
  onComplete = async () => {
    await this.props.context.updateLoggedUser(null);
    return this.props.navigation.navigate(ROUTE_NAMES.LOGIN);
  };

  render() {
    return (
      <LogoutController>
        {({ logout }) => <CallLogout logout={logout} onComplete={this.onComplete} />}
      </LogoutController>
    );
  }
}

export const LogoutConnector = withAppContext(LogoutC);