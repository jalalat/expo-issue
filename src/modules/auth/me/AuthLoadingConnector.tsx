import * as React from 'react';
import { View } from "react-native";
import { NavigationScreenProps } from 'react-navigation';

import { ROUTE_NAMES } from '@bakbak/common';
import { withMeQuery, IMeQuery, MeQuery_me } from '@bakbak/controller';
import { setLocalStorageItem } from '../../../util/LocalStorage';
import { LOCAL_STORAGE_LOGGED_USER } from '../../../constants';


class AuthLoadingConnector extends React.Component<NavigationScreenProps & IMeQuery> {
  constructor(props: any) {
    super(props);
    const { loggedUser, meLoading } = props;
    this.displayRoute(loggedUser, meLoading);
  }
  displayRoute = async (loggedUser: MeQuery_me | undefined, meLoading: boolean) => {
    if (meLoading) {
      return null;
    }
    else if (!loggedUser) {
      await setLocalStorageItem(LOCAL_STORAGE_LOGGED_USER, null);
      return this.props.navigation.navigate(ROUTE_NAMES.AUTH);
    }
    else {
      await setLocalStorageItem(LOCAL_STORAGE_LOGGED_USER, loggedUser);
      return this.props.navigation.navigate(ROUTE_NAMES.APP);
    }
  }
  componentDidUpdate(prevProps: any) {
    if (this.props.meLoading !== prevProps.meLoading) {
      this.displayRoute(this.props.loggedUser, this.props.meLoading)
    }
  }
  // componentWillReceiveProps(props: any) {
  //   const { loggedUser, meLoading } = props;
  //   if (props.meLoading !== this.props.meLoading) {
  //     this.displayRoute(loggedUser, meLoading);
  //   }
  // }
  render() {
    return <View />;
  }
}
export default withMeQuery(AuthLoadingConnector);
