import * as React from 'react';
import { InputField } from "../../../util/InputField";
import { Field } from "formik";
import { View } from 'react-native';

export const Page1 = () => (
  <View>
    <Field
      name="email"
      placeholder="email"
      component={InputField}
      containerStyle={{ width: '100%' }}
      autoCaptialize="none"
      keyboardType="email-address"
      textContentType="email"
      on
    />
    <Field
      name="userName"
      placeholder="User Name"
      component={InputField}
      containerStyle={{ width: '100%' }}
      autoCaptialize="none"
      keyboardType="email-address"
      textContentType="username"
    />
  </View>
);