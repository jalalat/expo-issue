import * as React from 'react';
import { InputField } from "../../../util/InputField";
import { Field } from "formik";
import { View, Text } from 'react-native';
import { PictureField } from '../../../util/PictureField';

export const Page2 = () => (
  <View>
    <Field
      name="password"
      secureTextEntry={true}
      placeholder="password"
      component={InputField}
      containerStyle={{ width: '100%' }}
      autoCaptialize="none"
    />
    <Text style={{ marginBottom: 30 }} />
    <Field
      name="picture"
      title="Upload a Picture"
      component={PictureField as any}
    />
    <Text style={{ marginBottom: 30 }} />

  </View >
);