import * as React from 'react';
import { NavigationScreenProps } from 'react-navigation';


import { ROUTE_NAMES } from '@bakbak/common';
import { RegisterMutation_register_user, IWithRegister, RegisterController } from '@bakbak/controller';

import { RegisterView } from './RegisterView';
import { AppContextProps, withAppContext } from '../../../context/AppContext';

export class RegisterC extends React.PureComponent<NavigationScreenProps & IWithRegister & AppContextProps> {

	onComplete = (user: RegisterMutation_register_user | null) => {
		if (user) {
			this.props.context.updateLoggedUser(user);
			this.props.navigation.navigate(ROUTE_NAMES.CATEGORY_LIST);
		} else {
			// TODO: did not get any response back.
		}
	}

	gotoLogin = () => {
		this.props.navigation.navigate(ROUTE_NAMES.LOGIN);
	}

	render() {
		return (
			<RegisterController>
				{({ submit }) =>
					<RegisterView
						submit={submit}
						gotoLogin={this.gotoLogin}
						onComplete={this.onComplete}
					/>
				}
			</RegisterController>
		);
	}
}

export const RegisterConnector = withAppContext(RegisterC);
