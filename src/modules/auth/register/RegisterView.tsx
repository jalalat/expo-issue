import * as React from "react";
import { View, Text, TouchableOpacity, KeyboardAvoidingView, StyleSheet } from "react-native";
import { Card, Button } from "react-native-elements";
import { withFormik, FormikProps, Field } from "formik";
import { MaterialIcons } from '@expo/vector-icons';

import { userValidationSchema } from '@bakbak/common';
import { RegisterMutationVariables, RegisterMutation_register, RegisterMutation_register_user } from '@bakbak/controller';

import { InputField } from "../../../util/InputField";
// import { PictureField } from "../../../util/PictureField";
import { THEME_COLOR } from "../../../constants";

interface FormValues {
  email: string;
  userName: string;
  password: string;
}

interface Props {
  submit: (values: RegisterMutationVariables) => Promise<RegisterMutation_register | null>
  gotoLogin: () => void;
  onComplete: (user: RegisterMutation_register_user | null) => void;
}

interface State {
  secureText: boolean
}

class RegisterForm extends React.PureComponent<FormikProps<FormValues> & Props, State> {
  state = { secureText: true };
  toggleSecureText = () => {
    this.setState((state) => ({ secureText: !state.secureText }));
  }
  renderRegisterForm = () => (
    <View style={{}}>
      <Field
        name="email"
        placeholder="Email"
        component={InputField}
        containerStyle={{ width: '100%', borderBottomWidth: 1 }}
        autoCaptialize="none"
        keyboardType="email-address"
      />
      <Field
        name="userName"
        placeholder="User Name"
        component={InputField}
        containerStyle={{ width: '100%', borderBottomWidth: 1 }}
        autoCaptialize="none"
      />
      <View style={{ display: 'flex', flexDirection: 'row', justifyContent: "space-around" }}>
        <View style={{ width: "100%" }}>
          <Field
            name="password"
            placeholder="Password"
            secureTextEntry={this.state.secureText}
            component={InputField}
            style={{ paddingRight: 20 }}
            containerStyle={{ width: '100%', borderBottomWidth: 1 }}
          />
        </View>
        <TouchableOpacity onPress={this.toggleSecureText} style={styles.secureTextButtonStyle}>
          <MaterialIcons name="remove-red-eye" size={20} color={THEME_COLOR} />
        </TouchableOpacity>
      </View>
      <Text style={{ marginBottom: 30 }} />
      {/* <Field
        name="picture"
        title="Upload a Picture"
        component={PictureField as any}
      /> */}
      <Button title="Submit" onPress={this.props.handleSubmit as any}
        buttonStyle={{ backgroundColor: THEME_COLOR, marginTop: 20 }}
        fontSize={16}
        fontWeight={"600"}
      />
    </View>
  );

  render() {
    return (
      <KeyboardAvoidingView style={{ flex: 1, display: 'flex', justifyContent: 'center' }} behavior="padding">
        <Card>
          <Text style={{ fontSize: 20, marginBottom: 10, color: THEME_COLOR }}>Register</Text>
          {this.renderRegisterForm()}
          <TouchableOpacity onPress={this.props.gotoLogin} style={{ paddingTop: 20, flexDirection: 'row', marginLeft: 15 }}>
            <Text>Existing User </Text>
            <Text style={{ color: THEME_COLOR }}>Login Here</Text>
          </TouchableOpacity>
        </Card>
      </KeyboardAvoidingView>
    );
  }
}

export const RegisterView = withFormik<Props, FormValues>({
  validationSchema: userValidationSchema,
  mapPropsToValues: () => ({ email: "", userName: "", password: "" }),
  handleSubmit: async (values, { props, setErrors }) => {
    console.log('value', values);
    const response = await props.submit(values);
    if (response && response.errors) {
      setErrors(response.errors as any);
    } else if (response) {
      props.onComplete(response.user);
    }
  }
})(RegisterForm);

const styles = StyleSheet.create({
  secureTextButtonStyle: {
    marginLeft: 'auto',
    marginRight: 15
  }
});  