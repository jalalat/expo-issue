import * as React from 'react';
import { View, Text, TouchableOpacity, Modal, Image } from "react-native";

import { CreateCommentController, CreateCommentMutation_createComment_comment } from '@bakbak/controller';

import { CreateCommentView } from './CreateCommentView';
import { withAppContext, AppContextProps } from '../../../context/AppContext';

interface Props {
  postId: string;
  textToReply: string;
}

export class CreateCommentC extends React.PureComponent<Props & AppContextProps> {
  toggleCreateCommentModal = () => {
    this.props.context.toggleCommentModal();
  }
  onComplete = (comment: CreateCommentMutation_createComment_comment | null) => {
    // this.props.navigation.navigate(ROUTE_NAMES.CATEGORY_LIST);
    // TODO: do somehting after comment creation.
    this.toggleCreateCommentModal();
  }

  render() {
    return (
      <View>
        <TouchableOpacity onPress={this.toggleCreateCommentModal} style={{ marginBottom: 5, marginTop: 10 }}>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{ height: 30, width: 30, borderRadius: 15, marginLeft: 10 }}>
              <Image style={{ height: 30, width: 30, borderRadius: 15 }} source={{ uri: 'http://www.free-avatars.com/data/media/37/cat_avatar_0597.jpg' }} />
            </View>
            <View>
              <Text style={{ color: 'grey', marginLeft: 15 }}>Comment Here....</Text>
              {/* <View style={{ marginTop: 5, borderWidth: 0.5, borderColor: 'grey', marginBottom: 15, marginLeft: 15 }} /> */}
            </View>
          </View>
        </TouchableOpacity>

        <View style={{ marginTop: 5, borderWidth: 0.5, borderColor: 'grey', marginLeft: -15 }} />
        <Modal
          animationType={'slide'}
          transparent={true}
          visible={this.props.context.showCommentModal}
          onRequestClose={this.toggleCreateCommentModal}
        >
          <CreateCommentController>
            {({ submit }) =>
              <CreateCommentView
                postId={this.props.postId}
                submit={submit}
                onComplete={this.onComplete}
                textToReply={this.props.textToReply}
                toggleCreateCommentModal={this.toggleCreateCommentModal}
              />
            }
          </CreateCommentController>
        </Modal>
      </View>
    );
  }
}

export const CreateCommentConnector = withAppContext(CreateCommentC);