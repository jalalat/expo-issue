import * as React from 'react';
import { View, Text, TouchableOpacity, Modal, Image } from "react-native";
import { Ionicons } from '@expo/vector-icons';

import { CreateCommentConnector } from './CreateCommentConnector';

interface Props {
  postId: string;
  commentsCount: number;
}
interface State {
  createCommentModalShow: boolean;
}

export class CreateCommentModal extends React.PureComponent<Props, State> {
  state = { createCommentModalShow: false };
  toggleCreateCommentModal = () => {
    this.setState((state) => ({ createCommentModalShow: !state.createCommentModalShow }));
  }
  render() {
    return (
      <View>
        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', marginBottom: 15 }}>
          <Text style={{ marginLeft: 15, fontSize: 16 }}>Comments {this.props.commentsCount}</Text>
          <Ionicons name="md-options" size={24} style={{ marginRight: 20 }} />
        </View>
        <TouchableOpacity onPress={this.toggleCreateCommentModal} style={{ marginBottom: 5 }}>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{ height: 30, width: 30, borderRadius: 15, marginLeft: 10 }}>
              <Image style={{ height: 30, width: 30, borderRadius: 15 }} source={{ uri: 'http://www.free-avatars.com/data/media/37/cat_avatar_0597.jpg' }} />
            </View>
            <View>
              <Text style={{ color: 'grey', marginLeft: 15 }}>Comment Here....</Text>
              {/* <View style={{ marginTop: 5, borderWidth: 0.5, borderColor: 'grey', marginBottom: 15, marginLeft: 15 }} /> */}
            </View>
          </View>
        </TouchableOpacity>
        <View style={{ marginTop: 5, borderWidth: 0.5, borderColor: 'grey', marginLeft: -15 }} />
        <Modal
          animationType={'slide'}
          transparent={true}
          visible={this.state.createCommentModalShow}
          onRequestClose={this.toggleCreateCommentModal}
        >
          <CreateCommentConnector
            postId={this.props.postId}
            toggleCreateCommentModal={this.toggleCreateCommentModal}
          />
        </Modal>
      </View>
    );
  }
}