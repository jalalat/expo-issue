import * as React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, KeyboardAvoidingView, Platform, ScrollView } from 'react-native';
import { Field, withFormik, FormikProps } from 'formik';
import { MaterialIcons } from '@expo/vector-icons';

import { commentTextValidation } from '@bakbak/common';
import { CreateCommentMutation_createComment, CreateCommentMutation_createComment_comment } from '@bakbak/controller';

import { InputField } from '../../../util/InputField';
import { THEME_COLOR } from '../../../constants';
import { Button } from 'react-native-elements';

interface FormValues {
  text: string;
}
interface Props {
  postId: string;
  textToReply: string;
  submit: (values: { commentInput: { text: string; postId: string } }) => Promise<CreateCommentMutation_createComment | null>;
  onComplete: (comment: CreateCommentMutation_createComment_comment | null) => void;
  toggleCreateCommentModal: () => void;
}

export class CreateCommentForm extends React.PureComponent<FormikProps<FormValues> & Props> {
  render() {
    const { toggleCreateCommentModal } = this.props;
    return (
      <View style={styles.outerContainer}>
        <View style={{ backgroundColor: '#fff', height: '70%' }}>
          <KeyboardAvoidingView
            style={{ flex: 1, marginBottom: 30 }}
            behavior={(Platform.OS === 'ios') ? "padding" : undefined}
          >
            <View style={{ flexDirection: 'row', justifyContent: "space-between", marginTop: 10 }}>
              <TouchableOpacity onPress={toggleCreateCommentModal} style={{ marginLeft: 15 }}>
                <MaterialIcons name="arrow-back" color={"black"} size={25} />
              </TouchableOpacity>
              <Button
                title="Submit"
                onPress={this.props.handleSubmit}
                fontSize={16}
                fontWeight={"600"}
                buttonStyle={{ height: 25, width: 100, backgroundColor: THEME_COLOR }}
              />
              <Button
                title="Cancel"
                onPress={toggleCreateCommentModal}
                fontSize={16}
                fontWeight={"600"}
                buttonStyle={{ height: 25, width: 100 }}
              />
            </View>
            <ScrollView>
              <View><Text>{this.props.textToReply}</Text></View>
              <Field
                name="text"
                placeholder="Start typing reply here"
                placeholderTextColor="grey"
                autoCaptialize="none"
                multiline={true}
                component={InputField}
                containerStyle={{ marginBottom: 10 }}
              />
            </ScrollView>
          </KeyboardAvoidingView>
        </View>
      </View>
    );
  }
}

export const CreateCommentView = withFormik<Props, FormValues>({
  validationSchema: commentTextValidation,
  mapPropsToValues: () => ({ text: "" }),
  handleSubmit: async (values, { props, setErrors }) => {
    const formInput = { commentInput: { ...values, postId: props.postId } };
    const response = await props.submit(formInput);
    if (response && response.errors) {
      setErrors(response.errors as any);
    } else if (response) {
      props.onComplete(response.comment);
    }
  }
})(CreateCommentForm);

const styles = StyleSheet.create({
  outerContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: '#00000080',
  }
})