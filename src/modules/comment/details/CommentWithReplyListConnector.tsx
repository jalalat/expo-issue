import * as React from "react";
import { Text, View, TouchableOpacity, Modal } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';

import { CommentWithReplyListController, CreateReplyController } from "@bakbak/controller";
import { ReplyListView } from "../../reply/list/ReplyListView";

interface Props {
  commentId: string;
}

interface State {
  showTotalReplies: boolean;
};

export class CommentWithReplyListConnector extends React.PureComponent<Props, State> {
  state = { showTotalReplies: false };
  unsubscribe: () => void;

  componentWillUnmount() {
    this.unsubscribe();
  }

  toggleTotalReplies = () => {
    this.setState((prevState: any) => ({
      showTotalReplies: !prevState.showTotalReplies
    }));
  }

  render() {
    const { commentId } = this.props;
    return (
      <CommentWithReplyListController commentId={commentId}>
        {(data) => {
          const { commentWithReplies, commentWithRepliesLoading, subscribe } = data;
          if (commentWithRepliesLoading || !commentWithReplies) {
            return null;
          }
          if (!this.unsubscribe) {
            this.unsubscribe = subscribe();
          }
          const { replies } = commentWithReplies;
          return (
            <View>
              <TouchableOpacity onPress={this.toggleTotalReplies}>
                <View style={{ flexDirection: 'row' }} >
                  <Text style={{ fontSize: 13 }}>
                    {replies ? replies.length : 0}
                  </Text>
                  <MaterialIcons name="comment" color="grey" size={20} />
                </View>
              </TouchableOpacity>
              <Modal
                animationType={'slide'}
                transparent={true}
                visible={this.state.showTotalReplies}
                onRequestClose={this.toggleTotalReplies}
              >
                <CreateReplyController>
                  {({ submit }) =>
                    <ReplyListView
                      commentId={commentId}
                      replyList={replies}
                      submit={submit}
                      closeReplyListModal={this.toggleTotalReplies}
                      onComplete={this.toggleTotalReplies}
                    />
                  }
                </CreateReplyController>
              </Modal>
            </View>
          );
        }}
      </CommentWithReplyListController>
    );
  }
}
