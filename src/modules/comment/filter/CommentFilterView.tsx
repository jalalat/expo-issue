import * as React from "react";
import { Text } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';

import { COMMENT_SORT } from '@bakbak/common';

import { THEME_COLOR } from '../../../constants';

interface Props {
  toggleModal: () => void;
  updateCommentSort: (option: string) => void;
  selectedOption: string;
}
export class CommentFilterView extends React.PureComponent<Props> {
  selectOption = (option: string) => {
    this.props.updateCommentSort(option);
    this.props.toggleModal();
  }
  render() {
    return (
      <Menu onSelect={this.selectOption}>
        <MenuTrigger>
          <Text style={{ color: THEME_COLOR }}>Sort </Text>
        </MenuTrigger>
        <MenuOptions>
          <CheckedOption checked={this.props.selectedOption === COMMENT_SORT[0].key} value={COMMENT_SORT[0].key} text={COMMENT_SORT[0].name} />
          <CheckedOption checked={this.props.selectedOption === COMMENT_SORT[1].key} value={COMMENT_SORT[1].key} text={COMMENT_SORT[1].name} />
        </MenuOptions>
      </Menu>
    )
  }
}

const CheckedOption = (props: any) => {
  const { checked } = props;
  return (
    <MenuOption value={props.value} style={{ flex: 1, flexDirection: 'row', margin: 2 }}>
      {checked && <MaterialIcons name="check" size={20} color={THEME_COLOR} />}
      <Text style={{ color: checked ? THEME_COLOR : 'black' }}> {props.text}</Text>
    </MenuOption>
  )
};