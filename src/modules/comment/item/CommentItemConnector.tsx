import * as React from 'react';

import { withCommentVoteChange, IWithCommentVoteChange, GetCommentListByPostIdQuery_getCommentListByPostId_comments } from "@bakbak/controller";
import { CommentItemView } from './CommentItemView';
import { AppContextProps, withAppContext } from '../../../context/AppContext';

interface Props {
  comment: GetCommentListByPostIdQuery_getCommentListByPostId_comments;
}

class CommentItemC extends React.PureComponent<Props & IWithCommentVoteChange & AppContextProps> {
  changeVote = async (likeDislike: number) => {
    const { commentVoteChange, comment } = this.props;

    const response = await commentVoteChange({ commentId: comment.id, likeDislike });
    if (response === null) {
      // TODO: Log this error.
      console.log('some error occured. Log it properly', response);
    } else if (response.errors) {
      console.log('some error occured. Log it properly', response.errors);
    } else {
      this.setState(() => ({ comment: response.comment }));
    }
  }
  render() {
    return (
      <CommentItemView
        comment={this.props.comment}
        changeVote={this.changeVote}
        loggedUserId={this.props.context.loggedUser ? this.props.context.loggedUser.id : null}
      />
    );
  }
}

export const CommentItemConnector = withAppContext(withCommentVoteChange(CommentItemC));
