import * as React from 'react';
import { View, Text } from 'react-native';

import { CommentListController } from "@bakbak/controller";

import { CommentListView } from './CommentListView';
import { COMMENT_LIMIT } from '../../../constants';
import { withAppContext, AppContextProps } from '../../../context/AppContext';

interface Props {
  postId: string;
}

class CommentListC extends React.PureComponent<Props & AppContextProps> {
  unsubscribe: () => void;

  render() {
    const { postId, context } = this.props;
    return (
      <CommentListController postId={postId} limit={COMMENT_LIMIT} sortBy={context.commentSort}>
        {(data) => {
          const { comments, commentListLoading, subscribe, loadMoreComments, hasMore } = data;
          if (commentListLoading) {
            return <View><Text>...loading</Text></View>;
          }
          if (!this.unsubscribe) {
            this.unsubscribe = subscribe();
          }
          return (
            <View style={{ flex: 1, flexDirection: 'column' }}>
              <CommentListView
                commentList={comments}
                fetchMore={loadMoreComments}
                hasMoreComments={hasMore}
              />
              {/* <Button onPress={this.unsubscribe} title="Unsubscribe from live comments" />				 */}
            </View>
          );
        }}
      </CommentListController>
    );
  }
}

export const CommentListConnector = withAppContext(CommentListC);
