import * as React from 'react';
import { FlatList } from 'react-native';

import { GetCommentListByPostIdQuery_getCommentListByPostId_comments } from "@bakbak/controller";
import { CommentItemConnector } from '../item/CommentItemConnector';


interface Props {
  commentList: GetCommentListByPostIdQuery_getCommentListByPostId_comments[];
  fetchMore: () => void;
  hasMoreComments: boolean;
}

export class CommentListView extends React.PureComponent<Props> {
  keyExtractor = (item: GetCommentListByPostIdQuery_getCommentListByPostId_comments, index: number) =>
    item.id + index;
  renderItem = ({ item }: { item: GetCommentListByPostIdQuery_getCommentListByPostId_comments }) => {
    return <CommentItemConnector comment={item} />;
  }

  loadMore = () => {
    if (this.props.hasMoreComments) {
      this.props.fetchMore();
    }
  }

  render() {
    const { commentList } = this.props;
    return (
      <React.Fragment>
        <FlatList
          data={commentList}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
          onEndReachedThreshold={0.2}
          onEndReached={this.loadMore}
        />
      </React.Fragment>
    );
  }
}