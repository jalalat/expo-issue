import * as React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';

import { CATEGORY_FILTER, DATE_FILTER } from '@bakbak/common';
import { OptionsComponent } from './OptionsComponent';
import { withHeaderContext, HeaderContext } from '../../context/HeaderContext';
import { THEME_COLOR } from '../../constants';


interface Props {
  headerContext: HeaderContext;
}

// interface State {
//   selectedCategoryOptions: string[];
//   selectedDateOptions: string[];
// }
export class FilterC extends React.PureComponent<Props> {
  // state = {
  //   selectedCategoryOptions: [] as string[],
  //   selectedDateOptions: [] as string[]
  // };
  updateCategoryOptions = (options: string[]) => {
    // this.setState(() => ({ selectedCategoryOptions: [...options] }));
    this.props.headerContext.updateCategoryFilter([...options]);
  }
  updateDateOptions = (options: string[]) => {
    // this.setState(() => ({ selectedDateOptions: [...options] }));
    this.props.headerContext.updateDateFilter([...options]);
  }
  renderCategoryFilter = () => {
    return (
      <View style={{ marginBottom: 40 }}>
        <View style={styles.optionTypeBackgroundStyle}>
          <Text style={styles.optionTypeStyle}>Categoires</Text>
        </View>
        <OptionsComponent
          options={CATEGORY_FILTER}
          selectedOptions={this.props.headerContext.categoryFilter}
          updateOptions={this.updateCategoryOptions}
          multiSelect={true}
        />
      </View>
    );
  }
  renderDateFilter = () => {
    return (
      <View style={{ marginBottom: 20 }}>
        <View style={styles.optionTypeBackgroundStyle}>
          <Text style={styles.optionTypeStyle}>Date</Text>
        </View>
        <OptionsComponent
          options={DATE_FILTER}
          selectedOptions={this.props.headerContext.dateFilter}
          updateOptions={this.updateDateOptions}
          multiSelect={false}
        />
      </View>
    );
  }

  // This ensures that Search Post List Component is called only when modal is closed. 
  toggleModal = () => {
    // this.props.headerContext.updateCategoryFilter([...this.state.selectedCategoryOptions]);
    // this.props.headerContext.updateDateFilter([...this.state.selectedDateOptions]);
    this.props.headerContext.toggleShowFilter();
  }
  render() {
    return (
      <View style={{ margin: 10 }}>
        <View style={{ flexDirection: 'row', justifyContent: "space-between" }}>
          <TouchableOpacity onPress={this.toggleModal}>
            <MaterialIcons name="arrow-back" color={THEME_COLOR} size={25} />
          </TouchableOpacity>
          <Text style={{ fontSize: 18, color: THEME_COLOR }}>
            Filter Posts
          </Text>
          <TouchableOpacity onPress={this.toggleModal}>
            <MaterialIcons name="close" color={THEME_COLOR} size={25} />
          </TouchableOpacity>
        </View>
        <View style={{ marginTop: 20 }}>
          {this.renderCategoryFilter()}
          {this.renderDateFilter()}
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  optionTypeStyle: {
    color: THEME_COLOR,
    fontWeight: "500",
    fontSize: 17,
  },
  optionTypeBackgroundStyle: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    backgroundColor: 'rgba(247,247,247,1.0)'
  },
});

export const FilterComponent = withHeaderContext(FilterC);