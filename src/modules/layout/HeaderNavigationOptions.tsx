import * as React from "react";
import { Button } from "react-native-elements";
import { View, StyleSheet, Modal } from "react-native";

import { HeaderConsumer, HeaderContext } from "../../context/HeaderContext";
import { SearchBarComponent } from "../shared/SearchBarComponent";
import { FilterComponent } from "./FilterComponent";
import { SortComponent } from "./SortComponent";

const navigationOptions = () => ({
  headerTitle:
    <HeaderConsumer>
      {(headerContext: HeaderContext) => (
        <View style={{ flex: 1, }}>
          <SearchBarComponent onChangeText={headerContext.updateSearchText} />
          <View style={styles.filterAndSortContainer}>
            <Button title="Filters"
              textStyle={styles.filterButtonTextStyle}
              buttonStyle={styles.filterButtonStyle}
              onPress={headerContext.toggleShowFilter}
            />
            <Modal
              animationType={'slide'}
              transparent={false}
              visible={headerContext.showFilter}
              onRequestClose={headerContext.toggleShowFilter}
            >
              <FilterComponent />
            </Modal>

            <Button title="Sort"
              textStyle={styles.filterButtonTextStyle}
              buttonStyle={styles.filterButtonStyle}
              onPress={headerContext.toggleShowSort}
            />
            <Modal
              animationType={'slide'}
              transparent={false}
              visible={headerContext.showSort}
              onRequestClose={headerContext.toggleShowSort}
            >
              <SortComponent />
            </Modal>
          </View>
        </View>
      )}
    </HeaderConsumer>,
  headerStyle: { height: 100 },
  headerTitleStyle: { fontWeight: "100" },
});

const styles = StyleSheet.create({
  filterAndSortContainer: {
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    flexDirection: 'row',
    backgroundColor: 'white',
    marginBottom: 10
  },
  filterButtonTextStyle: {
    color: "grey",
    fontWeight: "100"
  },
  filterButtonStyle: {
    backgroundColor: "white",
    borderColor: "lightgrey",
    borderWidth: 1,
    borderRadius: 7,
    marginLeft: 10,
  }

});

export default navigationOptions;
