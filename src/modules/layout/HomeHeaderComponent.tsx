import * as React from 'react'
import { View, Image, Text, TouchableOpacity, Modal, StyleSheet } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';

import { SORT_TYPE } from '@bakbak/common';

import { OptionsComponent } from './OptionsComponent';
import { HeaderContext, withHeaderContext } from '../../context/HeaderContext';
import { FilterComponent } from './FilterComponent';
import { SearchBarComponent } from '../shared/SearchBarComponent';

interface Props {
  headerContext: HeaderContext;
}

class HomeHeaderC extends React.PureComponent<Props> {
  updateOptions = (options: string[]) => {
    this.props.headerContext.updateSortType(options[0]);
  }

  render() {
    const { headerContext } = this.props;
    return (
      <React.Fragment>
        <View style={styles.searchRowStyle}>
          <View style={{ flex: 1, flexDirection: 'row', marginTop: 5 }}>
            <Image source={require("../../../assets/images/icon.png")} style={{ width: 24, height: 24 }} />
            <View><Text style={{ fontSize: 16 }}> BakBak</Text></View>
          </View>
          <View style={{ flex: 3, marginTop: -12 }}>
            <SearchBarComponent />
          </View>
        </View>
        <View style={styles.headerRowstyle}>
          <View style={{ flexDirection: 'row' }}>
            <Text>Sort:</Text>
            <View style={{ marginTop: -12, marginLeft: -7 }}>
              <OptionsComponent
                options={SORT_TYPE}
                selectedOptions={[headerContext.sortType]}
                updateOptions={this.updateOptions}
                multiSelect={false}
              />
            </View>

          </View>
          <View>
            <TouchableOpacity onPress={headerContext.toggleShowFilter} style={{ flex: 1, flexDirection: 'row' }}>
              <MaterialIcons name="filter-list" size={25} />
              <Text> Filter</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View>
          {headerContext.displayPullToRefresh &&
            <TouchableOpacity onPress={headerContext.toggleDisplayPullToRefresh}>
              <Text style={{ textAlign: 'center', color: 'grey', padding: 5, fontSize: 12, paddingTop: 10 }}>
                Pull to Refresh. Click to ignore this message.
              </Text>
            </TouchableOpacity>
          }
        </View>
        <View>
          <Modal
            animationType={'slide'}
            transparent={false}
            visible={headerContext.showFilter}
            onRequestClose={headerContext.toggleShowFilter}
          >
            <FilterComponent />
          </Modal>
        </View>
      </React.Fragment>
    );
  }
}

const styles = StyleSheet.create({
  headerRowstyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
    marginBottom: 10,
  },
  searchRowStyle: {
    flex: 4,
    flexDirection: 'row',
    marginLeft: 10,
    marginRight: 10,
  }
});

export const HomeHeaderComponent = withHeaderContext(HomeHeaderC);
