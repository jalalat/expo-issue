import * as React from 'react';
import { StyleSheet, View } from 'react-native';
import { Badge } from 'react-native-elements';

import { OptionType } from '@bakbak/common';
import { THEME_COLOR } from '../../constants';

interface Props {
  options: OptionType[];
  updateOptions: (options: string[]) => void;
  selectedOptions: string[] | null;
  multiSelect: boolean;
}

export class OptionsComponent extends React.PureComponent<Props> {

  updateOptions = (option: string) => {
    let { selectedOptions } = this.props;
    if (selectedOptions === null) {
      selectedOptions = [option];
    } else {
      const foundIndex = selectedOptions.findIndex((stateOption: string) => stateOption === option);

      if (option === "All" || selectedOptions.findIndex(stateOption => stateOption === "All") !== -1) {
        selectedOptions = [option];
      }
      // Option value exists and if there is more than 1 selected element -> remove that value from the list
      else if (foundIndex !== -1 && selectedOptions.length > 1) {
        selectedOptions.splice(foundIndex, 1);
      }
      else if (foundIndex === -1) {
        // Option name does not exist. Just add the name and value into the list.
        if (this.props.multiSelect) {
          selectedOptions.push(option);
        }
        // Option name does not exist. Replace the value with whole list.      
        else {
          selectedOptions = [option];
        }
      }
    }
    this.props.updateOptions([...selectedOptions]);
  }

  renderItem = ({ item }: { item: OptionType }) => {
    let textStyle = styles.unselectedOptionStyle;
    let containerStyle = styles.containerStyle;
    const { selectedOptions } = this.props;
    if (selectedOptions && selectedOptions.find((stateOption: string) => stateOption === item.value)) {
      textStyle = styles.selectedOptionStyle;
      containerStyle = styles.selectedContainerStyle;
    }
    return (
      <View style={{ marginRight: 8 }} key={item.name}>
        <Badge
          value={item.name}
          containerStyle={containerStyle}
          textStyle={textStyle}
          onPress={() => this.updateOptions(item.value)}
        />
      </View>

    );
  }
  render() {
    const { options } = this.props;
    return (
      <View style={styles.tagOuterContainerStyle}>
        {options.map(item => this.renderItem({ item }))}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  selectedOptionStyle: {
    color: THEME_COLOR,
    fontWeight: "500",
  },
  unselectedOptionStyle: {
    color: "grey",
  },
  selectedContainerStyle: {
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: THEME_COLOR,
    marginTop: 10,
  },
  containerStyle: {
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: 'grey',
    marginTop: 10,
  },
  tagOuterContainerStyle: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginLeft: 15,
  },
});
