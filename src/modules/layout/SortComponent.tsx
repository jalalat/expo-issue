import * as React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';

import { DATE_SORT, SCORE_SORT } from '@bakbak/common';
import { OptionsComponent } from './OptionsComponent';
import { withHeaderContext, HeaderContext } from '../../context/HeaderContext';
import { THEME_COLOR } from '../../constants';

interface Props {
  headerContext: HeaderContext;
}

export class SortC extends React.PureComponent<Props> {

  updateScoreOptions = (options: string[]) => {
    this.props.headerContext.updateScoreSort([...options]);
  }
  updateDateOptions = (options: string[]) => {
    this.props.headerContext.updateDateSort([...options]);
  }
  renderDateSort = () => {
    return (
      <View style={{ marginBottom: 40 }}>
        <View style={styles.optionTypeBackgroundStyle}>
          <Text style={styles.optionTypeStyle}>Date</Text>
        </View>
        <OptionsComponent
          options={DATE_SORT}
          selectedOptions={this.props.headerContext.dateSort}
          updateOptions={this.updateDateOptions}
          multiSelect={false}
        />
      </View>
    );
  }
  renderScoreSort = () => {
    return (
      <View style={{ marginBottom: 20, }}>
        <View style={styles.optionTypeBackgroundStyle}>
          <Text style={styles.optionTypeStyle}>Score</Text>
        </View>
        <OptionsComponent
          options={SCORE_SORT}
          selectedOptions={this.props.headerContext.scoreSort}
          updateOptions={this.updateScoreOptions}
          multiSelect={false}
        />
      </View>
    );
  }

  // This ensures that Search Component is called only when modal is closed. 
  toggleModal = () => {
    // this.props.headerContext.updateScoreSort([...this.state.selectedScoreOptions]);
    // this.props.headerContext.updateDateSort([...this.state.selectedDateOptions]);
    this.props.headerContext.toggleShowSort();
  }
  render() {
    return (
      <View style={{ margin: 10 }}>
        <View style={{ flexDirection: 'row', justifyContent: "space-between" }}>
          <TouchableOpacity onPress={this.toggleModal}>
            <MaterialIcons name="arrow-back" color={THEME_COLOR} size={25} />
          </TouchableOpacity>
          <Text style={{ fontSize: 18, color: THEME_COLOR }}>
            Sort Posts
          </Text>
          <TouchableOpacity onPress={this.toggleModal}>
            <MaterialIcons name="close" color={THEME_COLOR} size={25} />
          </TouchableOpacity>
        </View>
        <View style={{ marginTop: 20 }}>
          {this.renderDateSort()}
          {this.renderScoreSort()}
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  optionTypeStyle: {
    color: THEME_COLOR,
    fontWeight: "500",
    fontSize: 17,
  },
  optionTypeBackgroundStyle: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    backgroundColor: 'rgba(247,247,247,1.0)'
  },
});

export const SortComponent = withHeaderContext(SortC);