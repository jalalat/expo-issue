import * as React from "react";
import { NavigationScreenProps } from "react-navigation";

import { ROUTE_NAMES } from '@bakbak/common';
import { CreatePostController } from '@bakbak/controller';

import { CreatePostView } from "./CreatePostView";

export class CreatePostConnector extends React.PureComponent<NavigationScreenProps> {
  onComplete = () => {
    this.props.navigation.navigate(ROUTE_NAMES.CATEGORY_LIST);
  }
  render() {
    return (
      <CreatePostController>
        {({ submit }) => <CreatePostView onComplete={this.onComplete} submit={submit} />}
      </CreatePostController>
    );
  }
}
