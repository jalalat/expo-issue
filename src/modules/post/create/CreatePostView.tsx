import * as React from "react";
import { View, Text, ScrollView, KeyboardAvoidingView, Platform } from "react-native";
import { Button } from "react-native-elements";
import { withFormik, Field, FormikProps } from "formik";

import { postValidationSchema, CategoryTypes, POST_VALIDATIONS } from '@bakbak/common';

import { InputField } from "../../../util/InputField";
import { TagField } from "../../../util/TagField";
import { THEME_COLOR } from "../../../constants";
import { StatusBarComponent } from "../../shared/StatusBarComponent";

interface FormValues {
  title: string;
  details: string;
  categories: string[]
}
interface Props {
  onComplete: () => void;
  submit: (values: FormValues) => Promise<any | null>;
}

class CreatePostForm extends React.PureComponent<FormikProps<FormValues> & Props> {
  render() {
    return (
      <KeyboardAvoidingView
        style={{ flex: 1, backgroundColor: 'white' }}
        behavior={"padding"}
      >
        <ScrollView>
          <StatusBarComponent />
          <Text style={{ fontSize: 20, marginBottom: 10, marginLeft: 15, marginTop: 10, color: THEME_COLOR }}>
            Add Post
          </Text>
          <Field
            name="title"
            placeholder="Title"
            multiline={true}
            component={InputField}
            containerStyle={{ borderBottomWidth: 1, height: 50 }}
            autoFocus={true}
            maxLength={POST_VALIDATIONS.TITLE_MAX}
          />
          <Field
            name="details"
            placeholder="Details"
            component={InputField}
            multiline={true}
            containerStyle={{ borderBottomWidth: 1, height: 100 }}
            maxLength={POST_VALIDATIONS.DETAILS_MAX}
          />
          <View style={{ marginLeft: 15, marginRight: 15, marginBottom: 10 }}>
            <Text style={{ marginBottom: 7 }}> {"\n"} Choose a Category </Text>
            <Field
              name="categories"
              options={CategoryTypes}
              component={TagField as any}
            />
          </View>
          <Text style={{ marginBottom: 10 }} />
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: "space-between", width: '100%' }}>
            <Button
              title="Submit"
              onPress={this.props.handleSubmit}
              fontSize={16}
              fontWeight={"600"}
              buttonStyle={{ backgroundColor: THEME_COLOR, width: '80%' }}
            />
            <Button
              title="Cancel"
              onPress={this.props.onComplete}
              fontSize={16}
              fontWeight={"600"}
              buttonStyle={{ width: '80%' }}
            />
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

export const CreatePostView = withFormik<Props, FormValues>({
  validationSchema: postValidationSchema,
  mapPropsToValues: () => ({ title: "", details: "", categories: [] }),
  handleSubmit: async (values, { props, setErrors }) => {
    const response = await props.submit(values);
    if (response.errors) {
      setErrors(response.errors);
    } else {
      props.onComplete();
    }
  }
})(CreatePostForm);
