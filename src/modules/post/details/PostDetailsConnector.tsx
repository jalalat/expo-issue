import * as React from "react";
import { ScrollView, View, Text, TouchableOpacity } from "react-native";
import { NavigationScreenProps } from "react-navigation";

import { PostDetailsHeaderConnector } from "./PostDetailsHeaderConnector";
import { CommentListConnector } from "../../comment/list/CommentListConnector";
import { THEME_COLOR } from "../../../constants";
import { ContextState, AppConsumer } from "../../../context/AppContext";
import { CommentFilterView } from "../../comment/filter/CommentFilterView";

export class PostDetailsConnector extends React.PureComponent<NavigationScreenProps> {
	static navigationOptions = ({ navigation }: any) => ({
		headerTitle:
			<AppConsumer>
				{(context: ContextState) => (
					<View style={{ flex: 1, flexDirection: "row", justifyContent: "space-around" }}>
						<TouchableOpacity onPress={context.toggleCommentModal}>
							<Text style={{ color: THEME_COLOR }}>Comment</Text>
						</TouchableOpacity>
						<TouchableOpacity onPress={() => navigation.state.params.scrollViewRef.scrollToEnd({ animated: true })}>
							<Text style={{ color: THEME_COLOR }}>Goto End</Text>
						</TouchableOpacity>
						<CommentFilterView
							toggleModal={context.toggleCommentSortFilter}
							updateCommentSort={context.updateCommentSort}
							selectedOption={context.commentSort}
						/>
					</View>
				)}
			</AppConsumer>,
	});
	scrollViewRef: any;

	constructor(props: NavigationScreenProps) {
		super(props);
		this.scrollViewRef = React.createRef();
	}
	componentDidMount() {
		const { setParams } = this.props.navigation;
		setParams({ scrollViewRef: this.scrollViewRef });
		// setParams({ showCommentModal: this.props.appContext ? this.props.appContext.toggleCommentModal : null });
	}

	render() {
		const { navigation } = this.props;
		const postId = navigation.getParam('postId');
		const loggedUserId = navigation.getParam('loggedUserId');
		return (
			<ScrollView style={{ backgroundColor: "white" }} ref={c => this.scrollViewRef = c}>
				<PostDetailsHeaderConnector postId={postId} loggedUserId={loggedUserId} />
				<CommentListConnector postId={postId} />
			</ScrollView>
		);
	}
}