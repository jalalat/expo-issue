import * as React from "react";
import { View, Text } from "react-native";
import { Ionicons } from '@expo/vector-icons';


import { PostDetailsController, withPostVoteChange, IWithPostVoteChange } from "@bakbak/controller";

import { PostDetailsHeaderView } from "./PostDetailsHeaderView";
import { CreateCommentConnector } from "../../comment/create/CreateCommentConnector";

interface Props {
  postId: string;
  loggedUserId: string;
}

class PostDetailsHeaderC extends React.PureComponent<Props & IWithPostVoteChange> {

  changeVote = async (likeDislike: number) => {
    const { postVoteChange, postId } = this.props;

    const response = await postVoteChange({ postId, likeDislike });
    if (response === null) {
      // TODO: Log this error.
      console.log('some error occured. Log it properly', response);
    } else if (response.errors) {
      console.log('some error occured. Log it properly', response.errors);
    } else {
      this.setState(() => ({ post: response.post }));
    }
  }

  // TODO: change the post details to contain ComentCount
  renderTotalComments = (commentsCount: number) => (
    <View style={{ flex: 1, flexDirection: 'column' }}>
      <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
        <Text style={{ marginLeft: 15, fontSize: 16 }}>Comments {commentsCount}</Text>
        <Ionicons name="md-options" size={24} style={{ marginRight: 20 }} />
      </View>
      <CreateCommentConnector
        postId={this.props.postId}
        textToReply={''}
      />
    </View>
  );

  render() {
    const { postId, loggedUserId } = this.props;
    return (
      <PostDetailsController postId={postId}>
        {(data) => {
          const { postDetails, postDetailsLoading } = data;
          if (postDetailsLoading || !postDetails) {
            return <View />;
          }
          return (
            <React.Fragment>
              <PostDetailsHeaderView
                post={postDetails}
                changeVote={this.changeVote}
                loggedUserId={loggedUserId}
              />
              {this.renderTotalComments(postDetails.commentCount)}
            </React.Fragment>
          );
        }}
      </PostDetailsController>
    );
  }
}

export const PostDetailsHeaderConnector = withPostVoteChange(PostDetailsHeaderC);