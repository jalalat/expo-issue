import * as React from 'react';
import moment from 'moment';
import { View, Text, Image, StyleSheet } from 'react-native';

import { DATE_FORMAT } from '@bakbak/common';
import { GetPostDetailsByIdQuery_getPostDetailsById } from "@bakbak/controller";
import { THEME_COLOR } from '../../../constants';
import { VoteComponent } from '../../shared/VoteComponent';

interface Props {
	post: GetPostDetailsByIdQuery_getPostDetailsById;
	changeVote: (likeDislike: number) => void;
	loggedUserId: string
}

interface State {
	voteValue: number | null,
	voteChangeProgress: boolean,
};

export class PostDetailsHeaderView extends React.PureComponent<Props, State> {
	state = { voteValue: 0, voteChangeProgress: false };

	componentDidMount() {
		const { loggedUserId, post } = this.props;
		this.setupVoteValue(loggedUserId, post);
	}
	componentWillReceiveProps(nextProps: Props) {
		const { loggedUserId, post } = nextProps;
		this.setupVoteValue(loggedUserId, post);
	}

	setupVoteValue = (loggedUserId: string, post: GetPostDetailsByIdQuery_getPostDetailsById) => {
		if (loggedUserId && post && post.votes && post.votes.length !== 0) {
			const selectedVote = post.votes.find(vote => vote.author.id === loggedUserId);
			if (selectedVote) {
				this.setState(() => ({ voteValue: selectedVote.voteValue }));
			}
		}
	}

	onVoteChange = async (newVoteValue: number) => {
		if (this.state.voteChangeProgress) {
			return;
		}
		this.setState(() => ({ voteChangeProgress: true }));
		const oldVoteValue = this.state.voteValue;
		const changeInVoteValue = oldVoteValue + newVoteValue;

		this.setState(() => ({ voteValue: changeInVoteValue }));
		// Calling the mutation for changing the Vote of Post
		this.props.changeVote(newVoteValue);
		this.setState(() => ({ voteChangeProgress: false }));
	}

	render() {
		const { post } = this.props;
		return (
			<View style={{ margin: 15 }}>
				<View style={styles.profileContainerStyle}>
					<View style={{ flexDirection: 'row', }}>
						<Image source={{ uri: 'http://placehold.it/350x150' }} style={{ width: 35, height: 35 }} />
						<Text style={{ marginLeft: 10 }}>{post.author.userName}</Text>
					</View>
					<View>
						<Text style={{ color: "grey" }}> {moment(post.createdAt, DATE_FORMAT).fromNow()}</Text>
					</View>
				</View>
				<Text style={{ fontWeight: "bold" }}>{post.title} </Text>
				<Text style={{ marginBottom: 10 }}>{post.details} </Text>
				<View style={styles.buttonsContainer}>
					<VoteComponent
						likes={post.likes}
						dislikes={post.dislikes}
						loggedUserVoteValue={this.state.voteValue}
						onVoteChange={this.onVoteChange}
						voteChangeProgress={this.state.voteChangeProgress}
					/>
					<View style={{ flexDirection: 'row' }} >
						<Text style={styles.buttonTextStyle}>{post.score} Points </Text>
					</View>
				</View>
				<View style={{ marginTop: 5, borderWidth: 0.5, borderColor: 'grey', marginLeft: -15, marginRight: -15 }} />
			</View>
		);
	}
}

const styles = StyleSheet.create({
	profileContainerStyle: {
		display: "flex",
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginBottom: 10,
	},
	dateStyle: {
		fontSize: 12,
		color: 'grey',
	},
	buttonsContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	buttonTextStyle: {
		color: '#737373',
	},
	voteIconStyle: {
		color: THEME_COLOR,
	},
});