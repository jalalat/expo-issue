import * as React from "react";
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import { GetPostListQuery_getPostList } from "@bakbak/controller";

interface Props {
	post: GetPostListQuery_getPostList;
	onPostSelect: (postId: string) => void;
}

class PostItemView extends React.PureComponent<Props> {
	render() {
		const post = this.props.post;
		return (
			<TouchableOpacity onPress={() => this.props.onPostSelect(post.id)}>
				<View style={styles.cardContainer}>
					<View>
						<Text style={styles.cardTitleText} numberOfLines={2}>{post.title}</Text>
					</View>
					<View>
						<Text style={styles.cardDetailsText} numberOfLines={5}>{post.details}</Text>
					</View>

				</View>
			</TouchableOpacity>
		);
	}
}

const styles = StyleSheet.create({
	cardContainer: {
		width: 150,
		height: 130,
		borderWidth: 0.5,
		borderColor: '#f2f2f2',
		flexDirection: 'column',
		backgroundColor: 'white',
	},
	cardTitleContainer: {
		height: 85,
	},
	cardTitleText: {
		fontSize: 14,
		color: '#595959',
		paddingLeft: 4,
		paddingRight: 4,
		fontWeight: "bold"
	},
	cardDetailsText: {
		fontSize: 14,
		color: '#595959',
		paddingLeft: 4,
		paddingRight: 4,
	},
	cardAuthorContainer: {
	},
	cardAuthorText: {
		fontSize: 14,
		color: '#737373',
		paddingLeft: 4,
		paddingRight: 4,
		alignItems: 'flex-end',
		justifyContent: 'flex-end',
	},
	buttonsContainer: {
		flexDirection: 'row',
		justifyContent: 'space-around',
	},
	buttonTextStyle: {
		color: '#737373',
	}
});

export default PostItemView;