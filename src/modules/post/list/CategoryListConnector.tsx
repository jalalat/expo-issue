import * as React from "react";
import { ScrollView, RefreshControl, View, StyleSheet } from "react-native";
import { NavigationInjectedProps, withNavigation } from "react-navigation";

import { withPostList, IPostList } from '@bakbak/controller';
import { CategoryTypes } from '@bakbak/common';

import { HeaderContext, withHeaderContext } from "../../../context/HeaderContext";
import { PostListConnector } from "./PostListConnector";
import { THEME_COLOR } from "../../../constants";
import { HomeHeaderComponent } from "../../layout/HomeHeaderComponent";

interface Props {
  headerContext: HeaderContext;
}

class CategoryListC extends React.PureComponent<NavigationInjectedProps & IPostList & Props> {
  state = { refreshing: false };
  refetchList: any;

  constructor(props: any) {
    super(props);
    this.refetchList = [];
  }

  updateRefetchList = (refresh: () => void) => {
    this.refetchList.push(refresh);
  }
  onRefresh = () => {
    this.setState(() => ({ refreshing: true }));
    this.refetchList.map((refresh: any) => refresh());
    this.setState(() => ({ refreshing: false }));
  }

  refreshControl = () => (
    <RefreshControl
      refreshing={this.state.refreshing}
      onRefresh={this.onRefresh}
    />
  )
  updateOptions = (options: string[]) => {
    this.props.headerContext.updateSortType(options[0]);
  }

  render() {
    const { headerContext } = this.props;
    const { categoryFilter } = headerContext;

    let CategoryToShow: string[] = [] as string[];
    if (categoryFilter.length === 0 || (categoryFilter && categoryFilter.length !== 0 && categoryFilter[0] === "All")) {
      CategoryToShow = CategoryTypes;
    } else {
      CategoryTypes.map(category => {
        if (categoryFilter.findIndex((catFilter: string) => catFilter === category) !== -1) {
          CategoryToShow.push(category);
        }
      });
    }
    return (
      <View style={{ flex: 1, marginTop: 25 }} >
        <ScrollView
          refreshControl={this.refreshControl()}
          style={{ backgroundColor: 'white' }}
        >
          <HomeHeaderComponent />
          {CategoryToShow.map(category =>
            <PostListConnector
              category={category}
              key={category}
              refetchList={this.updateRefetchList}
            />)
          }
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  selectedButtonStyle: {
    backgroundColor: THEME_COLOR,
    width: '80%',
  },
  otherButtonStyle: {
    backgroundColor: 'grey',
    width: '80%',
  },
  filterButtonTextStyle: {
    color: "grey",
    fontWeight: "100"
  },
  filterButtonStyle: {
    backgroundColor: "white",
    borderColor: "lightgrey",
    borderWidth: 1,
    borderRadius: 7,
    marginLeft: 10,
    marginTop: 10,
  }
});

const CategoryListConnector = withNavigation(withHeaderContext(withPostList(CategoryListC)));
export { CategoryListConnector };