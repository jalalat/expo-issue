import * as React from "react";
import { View } from "react-native";
import { NavigationInjectedProps, withNavigation } from "react-navigation";

import { ROUTE_NAMES } from '@bakbak/common';
import { withPostList, IPostList, SearchPostListController } from '@bakbak/controller';

import PostListView from "./PostListView";
import { POST_LIST_LIMIT, LOCAL_STORAGE_LOGGED_USER } from "../../../constants";
import { HeaderContext, withHeaderContext } from "../../../context/HeaderContext";
import { getLocalStorageItem } from "../../../util/LocalStorage";

interface Props {
  category: string;
  refetchList: (refetch: () => void) => void;
  headerContext: HeaderContext;
}

class PostListC extends React.PureComponent<NavigationInjectedProps & IPostList & Props> {

  gotoPostDetails = async (postId: string) => {
    const user = await getLocalStorageItem(LOCAL_STORAGE_LOGGED_USER);
    if (user) {
      this.props.navigation.navigate(ROUTE_NAMES.POST_DETAILS, {
        postId,
        loggedUserId: user.id
      });
    } else {
      this.props.navigation.navigate(ROUTE_NAMES.AUTH);
    }
  }

  render() {
    const { headerContext, category } = this.props;
    return (
      <View style={{ backgroundColor: 'white' }}>
        <SearchPostListController
          text={headerContext.searchText}
          categoryFilter={[category]}
          dateFilter={headerContext.dateFilter}
          dateSort={headerContext.dateSort}
          scoreSort={headerContext.scoreSort}
          limit={POST_LIST_LIMIT}
        >
          {(data) => {
            const { posts, searchPostListLoading, hasMore, loadMorePosts, refetch } = data;

            this.props.refetchList(refetch);
            return (
              <PostListView
                postList={posts}
                hasMorePosts={hasMore}
                loadMore={loadMorePosts}
                postListLoading={searchPostListLoading}
                gotoPostDetails={this.gotoPostDetails}
                category={category}
              />
            )
          }}
        </SearchPostListController>
      </View>
    );
  }
}

const PostListConnector = withNavigation(withHeaderContext(withPostList(PostListC)));

export { PostListConnector };