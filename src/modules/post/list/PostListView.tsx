import * as React from "react";
import { View, StyleSheet, ActivityIndicator, FlatList, Text, TouchableOpacity } from "react-native";

import { SearchPostListQuery_searchPostList_posts } from "@bakbak/controller";

import PostItemView from "../item/PostItemView";
import { THEME_COLOR } from "../../../constants";

interface Props {
	postList: SearchPostListQuery_searchPostList_posts[] | null;
	hasMorePosts: boolean;
	loadMore: () => void;
	postListLoading: boolean;
	gotoPostDetails: (postId: string) => void;
	category: string;
}

class PostListView extends React.PureComponent<Props> {
	keyExtractor = (item: SearchPostListQuery_searchPostList_posts, index: number) => item.id;
	renderItem = ({ item }: { item: SearchPostListQuery_searchPostList_posts }) => {
		return (
			<PostItemView post={item} onPostSelect={this.props.gotoPostDetails} />
		);
	}
	// When user clicks on Refresh Button in Header
	onRefresh = () => {
		return null;
	}
	renderHeader = () => {
		if (!this.props.postList || this.props.postList.length === 0) {
			// return (
			// 	<View style={styles.headerStyle}>			
			// 		<TouchableOpacity onPress={this.onRefresh} >
			// 			<Text>{this.props.category}</Text>
			// 			<MaterialIcons name="refresh" color="blue" size={40} />
			// 			<Text> Refresh </Text>
			// 		</TouchableOpacity>
			// 	</View>
			// );
			return (
				<View style={styles.headerStyle}>
					<TouchableOpacity onPress={this.onRefresh} >
						<Text> No posts </Text>
					</TouchableOpacity>
				</View>
			);
		}
		return <View />;
	}
	renderFooter = () => {
		const { hasMorePosts } = this.props;
		if (hasMorePosts) {
			return (
				<View style={styles.footerStyle}>
					<ActivityIndicator animating size="large" />
				</View>
			);
		}
		return <View />;
	}
	onEndReached = () => {
		if (this.props.hasMorePosts) {
			this.props.loadMore();
		}
	}

	render() {
		const { postList } = this.props;

		return (
			<View style={styles.container}>
				<View style={styles.categoryView}>
					<Text style={styles.categoryTitle}> {this.props.category} </Text>
				</View>
				<FlatList
					horizontal
					showsHorizontalScrollIndicator={false}
					data={postList}
					renderItem={this.renderItem}
					keyExtractor={this.keyExtractor}
					onEndReachedThreshold={0.2}
					onEndReached={this.onEndReached}
					ListHeaderComponent={this.renderHeader}
					ListFooterComponent={this.renderFooter}
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {

	},
	categoryView: {
		paddingTop: 12,
		paddingLeft: 8,
		paddingRight: 10,
		paddingBottom: 5,
		backgroundColor: 'rgba(247,247,247,1.0)',
	},
	categoryTitle: {
		width: 130,
		borderColor: 'grey',
		borderBottomColor: 'white',
		borderRadius: 10,
		fontSize: 17,
		fontWeight: "500",
		color: THEME_COLOR
	},
	headerStyle: {
		width: 130,
		height: 130,
		borderWidth: 0.5,
		borderColor: '#f2f2f2',
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: 'white',
	},
	footerStyle: {
		width: 130,
		height: 130,
		borderWidth: 0.5,
		borderColor: '#f2f2f2',
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#e6eeff',
	},
});

export default PostListView;
