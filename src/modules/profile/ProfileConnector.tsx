import * as React from "react";
import { NavigationScreenProps } from 'react-navigation';

import { ROUTE_NAMES } from '@bakbak/common';
import { ProfileDetailsController, IProfileDetailsQuery, LogoutController } from '@bakbak/controller';

import { ProfileView } from "./ProfileView";
import { View, Text } from "react-native";
import { withAppContext, AppContextProps } from "../../context/AppContext";


export class ProfileC extends React.PureComponent<NavigationScreenProps & IProfileDetailsQuery & AppContextProps> {
  static navigationOptions = {
    header: null,
  };
  async componentDidMount() {
    const { loggedUser } = this.props.context;
    if (!loggedUser) {
      this.props.navigation.navigate(ROUTE_NAMES.LOGIN);
    }
  }
  gotoPostDetails = async (postId: string) => {
    const { loggedUser } = this.props.context;
    this.props.navigation.navigate(ROUTE_NAMES.PROFILE_POST_DETAILS, {
      postId,
      loggedUserId: loggedUser.id,
    });
  }

  updateLoggedUser = (profile: any) => {
    this.props.context.updateLoggedUser(profile);
  }

  gotoUdateProfile = () => {
    this.props.navigation.navigate(ROUTE_NAMES.EDIT_PROFILE);
  }

  onLogoutComplete = async () => {
    await this.props.context.updateLoggedUser(null);
    return this.props.navigation.navigate(ROUTE_NAMES.LOGIN);
  }
  render() {
    const { loggedUser } = this.props.context;
    if (!loggedUser) {
      return <View />
    }
    return (
      <ProfileDetailsController userId={loggedUser.id}>
        {(data) => {
          const { profile, profileLoading } = data;
          if (profileLoading || !profile) {
            return <View style={{ backgroundColor: 'white' }}><Text>...loading</Text></View>;
          }
          return (
            <LogoutController>
              {({ logout }) =>
                <ProfileView
                  profile={profile}
                  logout={logout}
                  gotoUdateProfile={this.gotoUdateProfile}
                  onLogoutComplete={this.onLogoutComplete}
                  gotoPostDetails={this.gotoPostDetails}
                  updateLoggedUser={this.updateLoggedUser}
                />
              }
            </LogoutController>

          );
        }}
      </ProfileDetailsController>
    )
  }
}

export const ProfileConnector = withAppContext(ProfileC);
