import * as React from 'react';
import { Text, TouchableOpacity, Image, View, StyleSheet, FlatList } from 'react-native';
import { MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';

import { GetProfileDetailsQuery_getProfileDetails } from '@bakbak/controller';

import { StatusBarComponent } from '../shared/StatusBarComponent';
import { THEME_COLOR } from '../../constants';
import PostItemView from '../post/item/PostItemView';
// import { InputField } from '../../../util/InputField';
// import { Field } from 'formik';

interface Props {
  profile: GetProfileDetailsQuery_getProfileDetails;
  logout: () => void;
  onLogoutComplete: () => void;
  gotoPostDetails: (postId: string) => void;
  gotoUdateProfile: () => void;
  updateLoggedUser: (profile: any) => void
}

export class ProfileView extends React.PureComponent<Props> {
  componentDidMount() {
    this.props.updateLoggedUser(this.props.profile);
  }
  onLogout = () => {
    this.props.logout();
    this.props.onLogoutComplete();
  }
  renderImageAndSettings = () => {
    const profilePic = 'http://placehold.it/350x150';
    const { profile } = this.props;
    return (
      <React.Fragment>
        <View style={styles.header}>
          <TouchableOpacity onPress={this.onLogout} style={styles.logoutStyle}>
            <Text style={{ color: 'white', fontWeight: '500' }}>Logout </Text>
            <MaterialCommunityIcons name="account-off" size={18} color='white' />
          </TouchableOpacity>
          <TouchableOpacity onPress={this.props.gotoUdateProfile} style={styles.editProfileStyle}>
            <Text style={{ color: 'white', fontWeight: '500' }}>Edit Profile </Text>
            <MaterialIcons name="edit" size={16} color='white' />
          </TouchableOpacity>
        </View>
        <Image source={{ uri: profile.pictureUrl ? profile.pictureUrl : profilePic }} style={styles.imageStyle} />
        <Text style={{ alignSelf: "center", fontSize: 18, marginTop: 85, marginBottom: 25 }}>
          {profile.userName}
        </Text>
      </React.Fragment>
    );

  };

  renderRow = () => {
    const { profile } = this.props;
    return (
      <View style={{ flexDirection: 'row', justifyContent: "space-around" }}>
        <View style={styles.ProfileBarStyle} >
          <Text style={styles.rowHeaderStyle}>
            {profile.posts ? profile.posts.length : <Text>Create some Posts</Text>}
          </Text>
          <Text style={styles.rowTextStyle}>
            {profile.posts ? <Text>POSTS</Text> : <Text />}
          </Text>
        </View>
        <View style={styles.ProfileBarStyle} >
          <Text style={styles.rowHeaderStyle}>
            {profile.score}
          </Text>
          <Text style={styles.rowTextStyle}>
            POINTS
            </Text>
        </View>
      </View>
    );
  }
  keyExtractor = (item: any, index: number) => item.id;
  renderItem = ({ item }: { item: any }) => {
    return (
      <PostItemView post={item} onPostSelect={this.props.gotoPostDetails} />
    );
  }
  renderPosts = () => {
    const { posts } = this.props.profile;
    return (
      <View style={{ marginTop: 10, marginLeft: 10 }}>
        <View style={{ marginTop: 10, marginBottom: 13, marginLeft: 5 }}>
          <Text style={styles.categoryTitle}>POSTS</Text>
        </View>
        <FlatList
          horizontal
          showsHorizontalScrollIndicator={false}
          data={posts}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
          onEndReachedThreshold={0}
        />
      </View>

    );
  }
  render() {
    return (
      <View style={{ backgroundColor: 'white', height: '100%' }}>
        <StatusBarComponent />
        {this.renderImageAndSettings()}
        {this.renderRow()}
        {this.props.profile.posts && this.renderPosts()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    display: 'flex',
    backgroundColor: THEME_COLOR,
    height: 150,
  },
  imageStyle: {
    alignSelf: "center",
    marginTop: 120,
    position: 'absolute',
    width: 130,
    height: 130,
    borderRadius: 15,
    borderWidth: 4,
    borderColor: "white",
    marginBottom: 60,
  },
  ProfileBarStyle: {
    alignSelf: "center",
    flex: 1,
    borderWidth: 0.5,
    borderRightWidth: 0,
    borderColor: 'lightgrey',
    padding: 10
  },
  rowHeaderStyle: {
    alignSelf: "center",
    fontSize: 15,
    fontWeight: "400"
  },
  rowTextStyle: {
    alignSelf: "center",
    color: "rgb(180,180,180)",
    fontWeight: "500"
  },
  categoryTitle: {
    fontWeight: "500",
    color: THEME_COLOR,
  },
  editProfileStyle: {
    display: 'flex',
    flexDirection: 'row',
    marginLeft: 'auto',
    marginTop: 'auto',
    marginRight: 5,
    marginBottom: 5
  },
  logoutStyle: {
    display: 'flex',
    flexDirection: 'row',
    marginLeft: 'auto',
    marginRight: 5,
    marginTop: 5
  }
});