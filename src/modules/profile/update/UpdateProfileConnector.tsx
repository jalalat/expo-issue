import * as React from 'react';
import { NavigationInjectedProps, withNavigation } from "react-navigation";

import { ROUTE_NAMES } from '@bakbak/common';
import { UpdateProfileController, RegisterMutation_register_user } from "@bakbak/controller";

import { UpdateProfileView } from './UpdateProfileView';
import { withAppContext, AppContextProps } from '../../../context/AppContext';


class UpdateProfileC extends React.PureComponent<NavigationInjectedProps & AppContextProps>{
  async componentDidMount() {
    const { context, navigation } = this.props;
    if (!context.loggedUser) {
      navigation.navigate(ROUTE_NAMES.AUTH);
    }
  }
  onComplete = (user: RegisterMutation_register_user | null) => {
    if (user) {
      this.props.context.updateLoggedUser(user);
      this.props.navigation.navigate(ROUTE_NAMES.MY_PROFILE);
    } else {
      // TODO: Need to do somehting here.
    }
  }

  onCancel = () => {
    this.props.navigation.navigate(ROUTE_NAMES.MY_PROFILE);
  }

  render() {
    return (
      <UpdateProfileController>
        {({ submit }) =>
          <UpdateProfileView
            loggedUser={this.props.context.loggedUser}
            submit={submit}
            onComplete={this.onComplete}
            onCancel={this.onCancel}
          />
        }
      </UpdateProfileController>
    );
  }
}

export const UpdateProfileConnector = withNavigation(withAppContext(UpdateProfileC));
