import * as React from "react";
import { View, Text, TouchableOpacity, KeyboardAvoidingView, StyleSheet, Image } from "react-native";
import { Card, Button } from "react-native-elements";
import { withFormik, FormikProps, Field } from "formik";
import { MaterialIcons } from '@expo/vector-icons';

import {
  UpdateProfileMutationVariables,
  UpdateProfileMutation_updateProfile,
  UpdateProfileMutation_updateProfile_user
} from "@bakbak/controller";

import { InputField } from "../../../util/InputField";
import { PictureField } from "../../../util/PictureField";
import { THEME_COLOR } from "../../../constants";

interface FormValues {
  email: string;
  userName: string;
  password: string;
  picture: any;
}

interface Props {
  loggedUser: UpdateProfileMutation_updateProfile_user;
  submit: (values: UpdateProfileMutationVariables) => Promise<UpdateProfileMutation_updateProfile | null>
  onComplete: (user: UpdateProfileMutation_updateProfile_user | null) => void;
  onCancel: () => void;
}

interface State {
  secureText: boolean
}

class UpdateProfileForm extends React.PureComponent<FormikProps<FormValues> & Props, State> {
  state = { secureText: true };
  toggleSecureText = () => {
    this.setState((state) => ({ secureText: !state.secureText }));
  }

  renderRegisterForm = () => {
    const { loggedUser, values } = this.props;
    return (
      <View style={{}}>
        <Field
          name="email"
          containerStyle={{ width: '100%', borderBottomWidth: 1 }}
          autoCaptialize="none"
          keyboardType="email-address"
          component={InputField}
        />
        <Field
          name="userName"
          placeholder={loggedUser.userName}
          containerStyle={{ width: '100%', borderBottomWidth: 1 }}
          autoCaptialize="none"
          component={InputField}
        />
        <View style={{ display: 'flex', flexDirection: 'row', justifyContent: "space-around" }}>
          <View style={{ width: "100%" }}>
            <Field
              name="password"
              placeholder="**********  (Enter new password)"
              secureTextEntry={this.state.secureText}
              style={{ paddingRight: 20 }}
              containerStyle={{ width: '100%', borderBottomWidth: 1 }}
              autoCaptialize="none"
              component={InputField}

            />
          </View>
          <TouchableOpacity onPress={this.toggleSecureText} style={styles.secureTextButtonStyle}>
            <MaterialIcons name="remove-red-eye" size={20} color={THEME_COLOR} />
          </TouchableOpacity>
        </View>
        <View style={{ marginTop: 10, display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
          <Image source={{ uri: values.picture.uri ? values.picture.uri : loggedUser.pictureUrl }} style={styles.imageStyle} />
          <Field
            name="picture"
            title="Change photo"
            component={PictureField as any}
          />
        </View>

        {/* {this.props.values.picture ? <Image source={this.props.values.picture.uri} /> : null} */}
        <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly' }}>
          <Button title="Submit" onPress={this.props.handleSubmit as any}
            buttonStyle={{ backgroundColor: THEME_COLOR, marginTop: 20, width: '90%' }}
            fontSize={16}
            fontWeight={"600"}
          />
          <Button title="Cancel" onPress={this.props.onCancel}
            buttonStyle={{ backgroundColor: 'grey', marginTop: 20, width: '90%' }}
            fontSize={16}
            fontWeight={"600"}
          />
        </View>

      </View>
    );
  }


  render() {
    return (
      <KeyboardAvoidingView style={{ flex: 1, display: 'flex', justifyContent: 'center' }} behavior="padding">
        <Card>
          <Text style={{ fontSize: 20, marginBottom: 10, color: THEME_COLOR }}>Update Profile</Text>
          {this.renderRegisterForm()}
        </Card>
      </KeyboardAvoidingView>
    );
  }
}

export const UpdateProfileView = withFormik<Props, FormValues>({
  validate: (values, props) => {
    const errors: any = {};
    const { loggedUser } = props;
    if (values.email === loggedUser.email &&
      values.userName === loggedUser.userName &&
      values.picture.uri === loggedUser.pictureUrl &&
      values.password === ""
    ) {
      errors.password = 'You did not make any changes';
    }
    return errors;
  },
  mapPropsToValues: (props) => {
    const { loggedUser } = props;
    return {
      email: loggedUser.email,
      userName: loggedUser.userName,
      password: "",
      picture: { uri: loggedUser.pictureUrl },
    };
  },
  handleSubmit: async (values, { props, setErrors }) => {
    const formInput = { ...values, userId: props.loggedUser.id };
    // console.log(values.picture);
    const response = await props.submit(formInput);
    if (response && response.errors) {
      setErrors(response.errors as any);
    } else if (response) {
      props.onComplete(response.user);
    }
  }
})(UpdateProfileForm);

const styles = StyleSheet.create({
  secureTextButtonStyle: {
    marginLeft: 'auto',
    marginRight: 15
  },
  imageStyle: {
    width: 130,
    height: 130,
    borderRadius: 15,
    borderWidth: 4,
    borderColor: "white",
  },
});  