import * as React from 'react';
import { Text, View, TouchableOpacity, Modal } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';

import { CreateReplyController } from '@bakbak/controller';

import { CreateReplyView } from './CreateReplyView';

interface Props {
  commentId: string;
  reply: any;
}

interface State {
  showCreateReplyModal: boolean;
}

export class CreateReplyConnector extends React.PureComponent<Props, State> {
  state = { showCreateReplyModal: false };

  toggleCreateReplyModal = () => {
    this.setState((prevState) => ({ showCreateReplyModal: !prevState.showCreateReplyModal }));
  }

  onComplete = () => {
    // TODO: do somehting after comment creation.
    this.toggleCreateReplyModal();
  }

  render() {
    const { commentId, reply } = this.props;
    return (
      <View>
        <TouchableOpacity onPress={this.toggleCreateReplyModal}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontSize: 13 }}>Reply </Text>
            <MaterialIcons name="reply" color="grey" size={20} />
          </View>
        </TouchableOpacity>
        <Modal
          animationType={'slide'}
          transparent={true}
          visible={this.state.showCreateReplyModal}
          onRequestClose={this.toggleCreateReplyModal}
        >
          <CreateReplyController>
            {({ submit }) =>
              <CreateReplyView
                commentId={commentId}
                submit={submit}
                onComplete={this.onComplete}
                reply={reply}
                toggleCreateReplyModal={this.toggleCreateReplyModal}
              />
            }
          </CreateReplyController>
        </Modal>
      </View>
    );
  }
}
