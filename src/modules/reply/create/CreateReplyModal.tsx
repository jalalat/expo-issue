import * as React from 'react';
import { View, Text, TouchableOpacity, Modal, Image } from "react-native";


interface Props {
  commentId: string;
}
interface State {
  createReplyModalShow: boolean;
}

export class CreateCommentModal extends React.PureComponent<Props, State> {
  state = { createReplyModalShow: false };
  toggleCreateReplyModal = () => {
    this.setState((state) => ({ createReplyModalShow: !state.createReplyModalShow }));
  }
  render() {
    return (
      <View>
        <TouchableOpacity onPress={this.toggleCreateReplyModal} style={{ marginBottom: 5 }}>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{ height: 30, width: 30, borderRadius: 15, marginLeft: 10 }}>
              <Image style={{ height: 30, width: 30, borderRadius: 15 }} source={{ uri: 'http://www.free-avatars.com/data/media/37/cat_avatar_0597.jpg' }} />
            </View>
            <View>
              <Text style={{ color: 'grey', marginLeft: 15 }}>Reply Here....</Text>
              {/* <View style={{ marginTop: 5, borderWidth: 0.5, borderColor: 'grey', marginBottom: 15, marginLeft: 15 }} /> */}
            </View>
          </View>
        </TouchableOpacity>
        <View style={{ marginTop: 5, borderWidth: 0.5, borderColor: 'grey', marginLeft: -15 }} />
        <Modal
          animationType={'slide'}
          transparent={true}
          visible={this.state.createReplyModalShow}
          onRequestClose={this.toggleCreateReplyModal}
        >
        </Modal>
      </View>
    );
  }
}