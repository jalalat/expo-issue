import * as React from 'react';
import moment from 'moment';
import { Button } from 'react-native-elements';
import { View, Text, TouchableOpacity, StyleSheet, KeyboardAvoidingView, Platform, ScrollView } from 'react-native';
import { Field, withFormik, FormikProps } from 'formik';
import { MaterialIcons } from '@expo/vector-icons';

import { replyTextValidation, DATE_FORMAT } from '@bakbak/common';

import { InputField } from '../../../util/InputField';
import { THEME_COLOR } from '../../../constants';

interface FormValues {
  text: string;
}
interface Props {
  commentId: string;
  reply: any;
  submit: (values: { text: string; commentId: string }) => Promise<any | null>;
  onComplete: () => void;
  toggleCreateReplyModal: () => void;
}

class CreateReplyForm extends React.PureComponent<FormikProps<FormValues> & Props> {

  renderReply = () => {
    const { reply } = this.props;
    return (
      <View style={styles.commentItemContainer}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <View>
            <Text style={{ fontWeight: 'bold' }}>{reply.author.userName}</Text>
          </View>
          <View>
            <Text style={{ fontSize: 12, color: 'grey' }}>
              {moment(reply.createdAt, DATE_FORMAT).fromNow()}
            </Text>
          </View>
        </View>

        <View>
          <Text>{reply.text}{'\n'}
          </Text>
        </View>
      </View>
    );
  }

  render() {
    const { toggleCreateReplyModal } = this.props;
    return (
      <View style={styles.outerContainer}>
        <View style={{ backgroundColor: '#fff', height: '70%' }}>
          <KeyboardAvoidingView
            style={{ flex: 1, marginBottom: 30 }}
            behavior={(Platform.OS === 'ios') ? "padding" : undefined}
          >
            <View style={{ flexDirection: 'row', justifyContent: "space-between", marginTop: 10 }}>
              <TouchableOpacity onPress={toggleCreateReplyModal} style={{ marginLeft: 15 }}>
                <MaterialIcons name="arrow-back" color={"black"} size={25} />
              </TouchableOpacity>
              <Button
                title="Submit"
                onPress={this.props.handleSubmit}
                fontSize={16}
                fontWeight={"600"}
                buttonStyle={{ height: 25, width: 100, backgroundColor: THEME_COLOR }}
              />
              <Button
                title="Cancel"
                onPress={toggleCreateReplyModal}
                fontSize={16}
                fontWeight={"600"}
                buttonStyle={{ height: 25, width: 100 }}
              />
            </View>
            <ScrollView>
              {this.renderReply()}
              <Field
                name="text"
                placeholder="Start typing reply here"
                placeholderTextColor="grey"
                autoCaptialize="none"
                multiline={true}
                component={InputField}
                containerStyle={{ marginBottom: 10 }}
              />
            </ScrollView>
          </KeyboardAvoidingView>
        </View>
      </View>
    );
  }
}

export const CreateReplyView = withFormik<Props, FormValues>({
  validationSchema: replyTextValidation,
  mapPropsToValues: () => ({ text: "" }),
  handleSubmit: async (values, { props, setErrors, resetForm }) => {
    const response = await props.submit({
      commentId: props.commentId,
      text: values.text
    });
    if (response && response.errors) {
      setErrors(response.errors as any);
    } else {
      props.onComplete();
      resetForm();
    }
  }
})(CreateReplyForm);

const styles = StyleSheet.create({
  outerContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: '#00000080',
  },
  commentItemContainer: {
    flexDirection: 'column',
    marginTop: 10,
    paddingBottom: 10,
    marginLeft: 15,
    marginRight: 15,
    borderBottomWidth: 0.5,
    borderBottomColor: 'grey',
  },
})