import * as React from 'react';

import { GetCommentWithRepliesByIdQuery_getCommentWithReplies_replies, withReplyVoteChange, IWithReplyVoteChange } from "@bakbak/controller";
import { AppContextProps, withAppContext } from '../../../context/AppContext';
import { ReplyItemView } from './ReplyItemView';

interface Props {
  reply: GetCommentWithRepliesByIdQuery_getCommentWithReplies_replies;
  commentId: string;
}

class ReplyItemC extends React.PureComponent<Props & IWithReplyVoteChange & AppContextProps> {
  changeVote = async (likeDislike: number) => {
    const { replyVoteChange, reply } = this.props;

    const response = await replyVoteChange({ replyId: reply.id, likeDislike });
    if (response === null) {
      // TODO: Log this error.
      console.log('some error occured. Log it properly', response);
    } else if (response.errors) {
      console.log('some error occured. Log it properly', response.errors);
    } else {
      this.setState(() => ({ reply: response.reply }));
    }
  }
  render() {
    const { context, commentId, reply } = this.props;
    return (
      <ReplyItemView
        commentId={commentId}
        reply={reply}
        changeVote={this.changeVote}
        loggedUserId={context.loggedUser ? context.loggedUser.id : null}
      />
    );
  }
}

export const ReplyItemConnector = withAppContext(withReplyVoteChange(ReplyItemC));
