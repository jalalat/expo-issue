import * as React from 'react';
import moment from 'moment';
import { Text, View, StyleSheet } from 'react-native';

import { DATE_FORMAT } from '@bakbak/common';
import { GetCommentWithRepliesByIdQuery_getCommentWithReplies_replies } from "@bakbak/controller";
import { VoteComponent } from '../../shared/VoteComponent';
import { CreateReplyConnector } from '../../reply/create/CreateReplyConnector';

interface Props {
  loggedUserId: string;
  commentId: string;
  reply: GetCommentWithRepliesByIdQuery_getCommentWithReplies_replies;
  changeVote: (likeDislike: number) => void;
}

interface State {
  voteValue: number | null;
  voteChangeProgress: boolean;
};

export class ReplyItemView extends React.PureComponent<Props, State> {
  state = { voteValue: 0, voteChangeProgress: false };

  componentWillMount() {
    this.setupVoteValue(this.props.loggedUserId, this.props.reply);
  }

  componentWillReceiveProps(nextProps: Props) {
    this.setupVoteValue(nextProps.loggedUserId, nextProps.reply);
  }

  setupVoteValue = (loggedUserId: string, reply: GetCommentWithRepliesByIdQuery_getCommentWithReplies_replies) => {
    if (loggedUserId && reply && reply.votes && reply.votes.length !== 0) {
      const selectedVote = reply.votes.find(vote => vote.author.id === loggedUserId);
      if (selectedVote) {
        this.setState(() => ({ voteValue: selectedVote.voteValue }));
      }
    }
  }

  onVoteChange = async (newVoteValue: number) => {
    if (this.state.voteChangeProgress) {
      return;
    }
    this.setState(() => ({ voteChangeProgress: true }));
    const oldVoteValue = this.state.voteValue;
    const changeInVoteValue = oldVoteValue + newVoteValue;

    this.setState(() => ({ voteValue: changeInVoteValue }));
    // Calling the mutation for changing the Vote of Post
    this.props.changeVote(newVoteValue);
    this.setState(() => ({ voteChangeProgress: false }));
  }

  render() {
    const { reply, commentId } = this.props;
    return (
      <View style={styles.commentItemContainer}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <View>
            <Text style={{ fontWeight: 'bold' }}>{reply.author.userName}</Text>
          </View>
          <View>
            <Text style={{ fontSize: 12, color: 'grey' }}>
              {moment(reply.createdAt, DATE_FORMAT).fromNow()}
            </Text>
          </View>
        </View>

        <View>
          <Text>{reply.text}{'\n'}
          </Text>
        </View>

        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} >
          <VoteComponent
            likes={reply.likes}
            dislikes={reply.dislikes}
            loggedUserVoteValue={this.state.voteValue}
            onVoteChange={this.onVoteChange}
            voteChangeProgress={this.state.voteChangeProgress}
          />
          <CreateReplyConnector commentId={commentId} reply={reply} />
        </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  commentItemContainer: {
    flexDirection: 'column',
    marginTop: 10,
    paddingBottom: 10,
    marginLeft: 15,
    marginRight: 15,
    borderBottomWidth: 0.5,
    borderBottomColor: 'grey',
  },
  buttonTextStyle: {
    fontSize: 13,
  },
  modalViewOuter: {
    flex: 1,
    marginTop: 20,
    backgroundColor: '#00000080',
    padding: 15,
    height: '97%',
  },
  modalViewInner: {
    backgroundColor: '#fff',
    height: '100%',
    borderRadius: 8,
  },
  replyTextStyle: {
    color: 'blue',
    alignSelf: 'center',
    fontWeight: 'bold',
  },
});