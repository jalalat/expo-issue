import * as React from 'react';
import { Button } from 'react-native-elements';
import { MaterialIcons } from '@expo/vector-icons';
import { View, Text, TouchableOpacity, StyleSheet, KeyboardAvoidingView, Platform, ScrollView, FlatList } from 'react-native';
import { Field, withFormik, FormikProps } from 'formik';

import { replyTextValidation } from '@bakbak/common';
import {
  GetCommentWithRepliesByIdQuery_getCommentWithReplies_replies, CreateReplyMutation_createReply,
} from "@bakbak/controller";
import { ReplyItemConnector } from '../item/ReplyItemConnector';
import { THEME_COLOR } from '../../../constants';
import { InputField } from '../../../util/InputField';

interface FormValues {
  text: string;
}

interface Props {
  commentId: string;
  // commentWithReplies: GetCommentWithRepliesByIdQuery_getCommentWithReplies;
  replyList: GetCommentWithRepliesByIdQuery_getCommentWithReplies_replies[] | null;
  closeReplyListModal: () => void;
  onComplete: () => void;
  submit: (values: { text: string; commentId: string }) => Promise<CreateReplyMutation_createReply | null>;
}

class ReplyListC extends React.PureComponent<FormikProps<FormValues> & Props> {

  keyExtractor = (item: GetCommentWithRepliesByIdQuery_getCommentWithReplies_replies, index: number) => item.id + index;
  renderItem = ({ item }: { item: GetCommentWithRepliesByIdQuery_getCommentWithReplies_replies }) => {
    return <ReplyItemConnector reply={item} commentId={this.props.commentId} />;
  }

  render() {
    const { replyList, closeReplyListModal, handleSubmit } = this.props;

    return (
      <View style={styles.outerContainer}>
        <View style={{ backgroundColor: '#fff', height: '70%' }}>
          <KeyboardAvoidingView
            style={{ flex: 1, marginBottom: 30 }}
            behavior={(Platform.OS === 'ios') ? "padding" : undefined}
          >
            <View style={{ flexDirection: 'row', justifyContent: "space-between", marginTop: 10, marginBottom: 10 }}>
              <TouchableOpacity onPress={closeReplyListModal} style={{ marginLeft: 15 }}>
                <MaterialIcons name="arrow-back" color={"black"} size={25} />
              </TouchableOpacity>
              <Button
                title="Submit"
                onPress={handleSubmit}
                fontSize={16}
                fontWeight={"600"}
                buttonStyle={{ height: 25, width: 100, backgroundColor: THEME_COLOR }}
              />
              <Button
                title="Cancel"
                onPress={closeReplyListModal}
                fontSize={16}
                fontWeight={"600"}
                buttonStyle={{ height: 25, width: 100 }}
              />
            </View>
            <ScrollView>
              <Field
                name="text"
                placeholder="Start typing reply here"
                placeholderTextColor="grey"
                autoCaptialize="none"
                multiline={true}
                component={InputField}
                containerStyle={{ marginBottom: 10 }}
              />
              <View style={{ borderBottomWidth: 0.5, borderBottomColor: 'grey', }} />
              <FlatList
                data={replyList}
                renderItem={this.renderItem}
                keyExtractor={this.keyExtractor}
              />
            </ScrollView>
          </KeyboardAvoidingView>
        </View>
      </View>
    );
  }
}

export const ReplyListView = withFormik<Props, FormValues>({
  validationSchema: replyTextValidation,
  mapPropsToValues: () => ({ text: "" }),
  handleSubmit: async (values, { props, setErrors }) => {
    const formInput = { ...values, commentId: props.commentId };
    const response = await props.submit(formInput);
    if (response && response.errors) {
      setErrors(response.errors as any);
    } else {
      props.onComplete();
    }
  }
})(ReplyListC);

const styles = StyleSheet.create({
  outerContainer: {
    flex: 1,
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: '#00000080',
  }
})