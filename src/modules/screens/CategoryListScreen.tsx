import * as React from "react";
import { NavigationScreenProps } from "react-navigation";

// import navigationOptions from "../layout/HeaderNavigationOptions";
import { CategoryListConnector } from "../post/list/CategoryListConnector";

export class CategoryListScreen extends React.PureComponent<NavigationScreenProps> {
  // static navigationOptions = navigationOptions;
  static navigationOptions = ({ navigation }: any) => ({
    header: null,
    headerStyle: null
  });

  render() {
    return (
      <CategoryListConnector />
    );
  }
}


