import * as React from 'react';
import { debounce } from "lodash";
import { View, Platform, StyleSheet } from "react-native";
import { SearchBar } from "react-native-elements";

import { SEARCH_DELAY, THEME_COLOR } from "../../constants";
import { withHeaderContext, HeaderContextProps } from '../../context/HeaderContext';


export class SearchBarC extends React.PureComponent<HeaderContextProps> {
  renderSearchBar = () => {
    const { headerContext } = this.props;
    if (Platform.OS === 'android') {
      return (
        <SearchBar
          clearIcon={{ color: THEME_COLOR }}
          icon={{ color: THEME_COLOR }}
          placeholder='Search here...'
          onChangeText={debounce(headerContext.updateSearchText, SEARCH_DELAY)}
          containerStyle={styles.containerStyle}
          inputStyle={{ backgroundColor: "white" }}
        />
      )
    } else if (Platform.OS === 'ios') {
      return (
        <SearchBar
          placeholder='Search here...'
          icon={{ color: THEME_COLOR }}
          onChangeText={debounce(headerContext.updateSearchText, SEARCH_DELAY)}
          containerStyle={styles.containerStyle}
          inputStyle={{ backgroundColor: "white" }}
        />
      )
    }
    return null;
  }
  render() {
    return (
      <View>
        {this.renderSearchBar()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    backgroundColor: "white",
    borderTopWidth: 0, borderBottomWidth: 0,
  }
});

export const SearchBarComponent = withHeaderContext(SearchBarC);
