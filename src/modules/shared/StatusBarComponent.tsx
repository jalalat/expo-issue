import * as React from 'react';
import { Constants } from 'expo';
import { StyleSheet, View } from 'react-native';

const styles = StyleSheet.create({
  statusBar: {
    backgroundColor: 'transparent',
    height: Constants.statusBarHeight,
  }
});

export class StatusBarComponent extends React.PureComponent {
  render() {
    return (
      <View style={styles.statusBar} />
    );
  }
}