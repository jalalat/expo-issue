import * as React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import { THEME_COLOR } from '../../constants';

interface Props {
  onVoteChange: (likeDislike: number) => void;
  likes: number;
  dislikes: number;
  loggedUserVoteValue: number;
  voteChangeProgress: boolean;
}

export class VoteComponent extends React.PureComponent<Props> {
  render() {
    const { likes, dislikes, loggedUserVoteValue, onVoteChange, voteChangeProgress } = this.props;
    return (
      <View style={{ flexDirection: 'row' }} >
        <View style={{ flexDirection: 'row' }} >
          <TouchableOpacity onPress={() => onVoteChange(1)} disabled={voteChangeProgress}>
            <View style={{ flexDirection: 'row' }}>
              <Text style={styles.buttonTextStyle} > {likes} </Text>
              <MaterialIcons name="keyboard-arrow-up" size={25} color="grey"
                style={this.props.loggedUserVoteValue === 1 && styles.voteIconStyle}
              />
            </View>
          </TouchableOpacity>
        </View>
        <View><Text>  </Text></View>
        <View style={{ flexDirection: 'row' }} >
          <TouchableOpacity onPress={() => onVoteChange(-1)} disabled={voteChangeProgress} >
            <View style={{ flexDirection: 'row' }}>
              <Text style={styles.buttonTextStyle} > {dislikes} </Text>
              <MaterialIcons name="keyboard-arrow-down" size={25} color="grey"
                style={loggedUserVoteValue === -1 && styles.voteIconStyle}
              />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  dateStyle: {
    fontSize: 12,
    color: 'grey',
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  buttonTextStyle: {
    color: '#737373',
  },
  voteIconStyle: {
    color: THEME_COLOR,
  },
});

