import * as React from 'react';

import { createBottomTabNavigator } from 'react-navigation';
import { MaterialIcons } from '@expo/vector-icons';

import { ROUTE_NAMES } from '@bakbak/common';

import PostRoutes from './PostRoutes';
import ProfileRoutes from './ProfileRoute';
import { THEME_COLOR } from '../constants';
import { CreatePostConnector } from '../modules/post/create/CreatePostConnector';

const AppRoutes = createBottomTabNavigator({
	[ROUTE_NAMES.HOME]: PostRoutes,
	[ROUTE_NAMES.CREATE_POST]: CreatePostConnector,
	// [ROUTE_NAMES.NOTIFICATION]: SearchBarComponent,
	[ROUTE_NAMES.PROFILE_ROUTE]: ProfileRoutes,
}, {
		navigationOptions: ({ navigation }) => {
			const { routeName } = navigation.state;
			let title = ''; let iconName = '';
			if (routeName === ROUTE_NAMES.HOME) {
				title = 'Home';
				iconName = 'home';
			} else if (routeName === ROUTE_NAMES.CREATE_POST) {
				title = 'Add Post';
				iconName = 'add';
			} else if (routeName === ROUTE_NAMES.PROFILE_ROUTE) {
				title = 'Profile';
				iconName = 'person';
			} else if (routeName === ROUTE_NAMES.NOTIFICATION) {
				title = 'Search';
				iconName = 'search';
			}

			return {
				title,
				tabBarIcon: ({ tintColor }: any) => {
					// if(iconName.toLowerCase() === "home"){
					// 	return (
					// 		<Image source={require("../../assets/images/icon.png")} style={{ width: 24, height: 24 }} />
					// 	);
					// } else {
					return <MaterialIcons name={iconName.toLowerCase()} color={tintColor} size={30} />;
					// }
				},
				tabBarOptions: { activeTintColor: THEME_COLOR },
			}
		}
	});

export default AppRoutes;

