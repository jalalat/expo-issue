import { createStackNavigator } from 'react-navigation';

import { ROUTE_NAMES } from '@bakbak/common';

import { LoginConnector } from '../modules/auth/login/LoginConnector';
import { LogoutConnector } from '../modules/auth/logout/LogoutConnector';
import { RegisterConnector } from '../modules/auth/register/RegisterConnector';

const AuthRoutes = createStackNavigator({
  [ROUTE_NAMES.LOGIN]: LoginConnector,
  [ROUTE_NAMES.REGISTER]: RegisterConnector,
  [ROUTE_NAMES.LOGOUT]: LogoutConnector
});

export default AuthRoutes;

