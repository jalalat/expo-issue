import { createStackNavigator, NavigationScreenProp, NavigationState } from 'react-navigation';

import { ROUTE_NAMES } from '@bakbak/common';

import { CategoryListScreen } from '../modules/screens/CategoryListScreen';
import { PostDetailsConnector } from '../modules/post/details/PostDetailsConnector';

const PostRoutes = createStackNavigator({
  [ROUTE_NAMES.CATEGORY_LIST]: CategoryListScreen,
  [ROUTE_NAMES.POST_DETAILS]: PostDetailsConnector,
});

PostRoutes.navigationOptions = ({ navigation }: { navigation: NavigationScreenProp<NavigationState> }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }
  return {
    tabBarVisible,
  };
};

export default PostRoutes;

