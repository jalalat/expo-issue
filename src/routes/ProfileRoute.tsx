import { createStackNavigator } from 'react-navigation';

import { ROUTE_NAMES } from '@bakbak/common';
import { ProfileConnector } from '../modules/profile/ProfileConnector';
import { UpdateProfileConnector } from '../modules/profile/update/UpdateProfileConnector';
import { PostDetailsConnector } from '../modules/post/details/PostDetailsConnector';

const ProfileRoutes = createStackNavigator({
  [ROUTE_NAMES.MY_PROFILE]: {
    screen: ProfileConnector,
    navigationOptions: {
      header: null
    },
  },
  [ROUTE_NAMES.EDIT_PROFILE]: UpdateProfileConnector,
  [ROUTE_NAMES.PROFILE_POST_DETAILS]: PostDetailsConnector,

});

export default ProfileRoutes;

