import { createSwitchNavigator } from 'react-navigation';

import { ROUTE_NAMES } from '@bakbak/common';

import AuthRoutes from './AuthRoutes';
import AppRoutes from './AppRoutes';
import AuthLoadingConnector from '../modules/auth/me/AuthLoadingConnector';

const Routes = createSwitchNavigator(
	{
		[ROUTE_NAMES.AUTH_LOADING]: AuthLoadingConnector,
		[ROUTE_NAMES.AUTH]: AuthRoutes,
		[ROUTE_NAMES.APP]: AppRoutes,
	},
	{
		initialRouteName: ROUTE_NAMES.AUTH_LOADING,
	}
);

export default Routes;

