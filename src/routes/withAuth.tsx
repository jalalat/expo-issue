import * as React from 'react';
// import { View, Text } from "react-native";
import { NavigationScreenProps } from 'react-navigation';

import { ROUTE_NAMES } from '@bakbak/common';
import { withMeQuery, IMeQuery } from '@bakbak/controller';

const withAuth = (WrappedComponent: any) => {
  class WithHOC extends React.Component<NavigationScreenProps & IMeQuery> {
    constructor(props: any) {
      super(props);
      // this.displayRoute(null, true);
    }
    // componentWillReceiveProps(props: any) {
    //   const { loggedUser, meLoading } = props;
    //   if(props.meLoading !== this.props.meLoading) {
    //     this.displayRoute(loggedUser, meLoading);
    //   }
    // }
    displayRoute = () => {
      const { loggedUser, meLoading } = this.props;
      console.log(meLoading, loggedUser);
      if (meLoading) {
        return null;
      }
      else if (!meLoading && !loggedUser) {
        return this.props.navigation.navigate(ROUTE_NAMES.AUTH);
      }
      else {
        console.log('else component');        
        return <WrappedComponent {...this.props} />;
      }
    }
    render() {
      return this.displayRoute();
    }
  }
   return withMeQuery(WithHOC);
};

export default withAuth;