import * as React from "react";
import { FieldProps } from "formik";
import { CheckBox } from 'react-native-elements';


export class CheckboxGroupField extends React.Component<FieldProps<any> & {options: string[]}> {
	onPress = (option: string, checked: boolean) => {
    const { 
      form: {setFieldValue}, 
      field: {name, value} } = this.props;
      console.log('', name, ...value);
      
    // form.setFieldValue(field.name, checked);
    if(checked){
      setFieldValue(name, value.filter((x: string) => x !== option));
    } else {
      setFieldValue(name, [...value, option]);
    }
	}
	render() {
		const {
      field: { name, value }, // { name, value, onChange, onBlur }
      options,
		} = this.props;
		return (
      <React.Fragment>
        {options.map(option => {
          const checked = value.includes(option);
          return (
            <CheckBox 
              key={option}
              title={option}
              checked={checked}
              onPress={() => this.onPress(option, checked)}
            />
          )
        })}  
      </React.Fragment>
		);
	}
}
