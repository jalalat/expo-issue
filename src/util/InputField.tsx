import * as React from "react";
import { FieldProps, ErrorMessage } from "formik";
import { Text } from "react-native";
import { FormInput } from "react-native-elements";

export class InputField extends React.Component<FieldProps<any>> {
	onChangeText = (text: string) => {
		const { form, field } = this.props;
		form.setFieldValue(field.name, text);
	}
	render() {
		const {
			field: { name, value }, // { name, value, onChange, onBlur }
			form, // : { touched, errors }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
			...props
		} = this.props;
		return (
			<React.Fragment>
				<FormInput
					{...props}
					onChangeText={this.onChangeText}
					value={value}
					underlineColorAndroid="transparent"
					inputStyle={{ color: 'black' }}
				/>
				<Text style={{ color: 'red', marginLeft: 15, marginRight: 15 }}>
					<ErrorMessage name={name} />
				</Text>
			</React.Fragment>
		);
	}
}
