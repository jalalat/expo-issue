import { AsyncStorage } from "react-native";

export const getLocalStorageItem = async (key: string) => {
  try {
    const serialisedValue = await AsyncStorage.getItem(key);
    if (serialisedValue === null) {
      return undefined;
    }
    return JSON.parse(serialisedValue);
  } catch (e) {
    return undefined;
  }
}

export const setLocalStorageItem = async (key: string, value: any) => {
  try {
    await AsyncStorage.setItem(key, JSON.stringify(value));
  } catch (e) {
    // ignore it
  }
}

export const removeLocalStorageItem = async (key: string) => {
  try {
    await AsyncStorage.removeItem(key);
  } catch (e) {
    // ignore it
  }
}