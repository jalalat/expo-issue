import * as React from "react";
import { FieldProps } from "formik";
import { ImagePicker, Permissions } from 'expo';
import { Button } from 'react-native-elements';
import { ReactNativeFile } from "apollo-upload-client";

export class PictureField extends React.Component<FieldProps<any> & { title: string }> {
  onPress = async () => {
    const { status } = await Permissions.getAsync(Permissions.CAMERA_ROLL);
    if (status !== "granted") {
      await Permissions.askAsync(Permissions.CAMERA_ROLL);
    }
    const imageUploadResult: any = await ImagePicker.launchImageLibraryAsync({});
    if (!imageUploadResult.cancelled) {
      const extension = imageUploadResult.uri.split('.').pop();
      const file = new ReactNativeFile({
        uri: imageUploadResult.uri,
        type: imageUploadResult.type + '/' + extension,
        name: 'picture',
      });
      const { form: { setFieldValue }, field: { name } } = this.props;
      setFieldValue(name, file);
    }
  }
  render() {
    const {
      field, // { name, value, onChange, onBlur }
      form,  // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
      ...props
    } = this.props;
    return (
      <Button
        {...props}
        onPress={this.onPress}
      />
    );
  }
}
