import * as React from "react";
import { FieldProps, ErrorMessage } from "formik";
import { Badge } from 'react-native-elements';
import { StyleSheet, View, Text } from "react-native";
import { THEME_COLOR } from "../constants";

export class TagField extends React.Component<FieldProps<any> & { options: string[] }> {
  onPress = (option: string) => {
    const {
      form: { setFieldValue },
      field: { name } } = this.props;
    setFieldValue(name, [option]);
  }
  render() {
    const {
      field: { name, value }, // { name, value, onChange, onBlur }
      options,
    } = this.props;
    return (
      <View style={styles.tagOuterContainerStyle}>
        {options.map(option => {

          const selected = value.includes(option);
          let containerStyle = styles.containerStyle;
          let textStyle = styles.textStyle;
          if (selected) {
            containerStyle = styles.selectedContainerStyle;
            textStyle = styles.selectedTextStyle;
          }
          return (
            <Badge
              key={option}
              value={option}
              containerStyle={containerStyle}
              textStyle={textStyle}
              onPress={() => this.onPress(option)}
            />
          )
        })}
        <Text style={{ color: 'red', marginRight: 15 }}>
          <ErrorMessage name={name} />
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  tagOuterContainerStyle: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    marginBottom: -10
  },
  selectedContainerStyle: {
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: THEME_COLOR,
    marginTop: 10
  },
  containerStyle: {
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: 'grey',
    marginTop: 10
  },
  textStyle: {
    color: 'grey',
  },
  selectedTextStyle: {
    color: THEME_COLOR,
  }
})